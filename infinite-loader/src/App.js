import React from 'react';
import logo from './logo.svg';
import './App.css';
import ACEGrid from './ACEGrid/ACEGrid';

function App() {
  return (
    <div className="App">
      <ACEGrid />
    </div>
  );
}

export default App;
