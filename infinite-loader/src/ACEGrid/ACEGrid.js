import React, { Component } from 'react';
import { AutoSizer, MultiGrid } from 'react-virtualized';
import axios from 'axios';
import './styles.scss';

const STYLE_TOP_LEFT_GRID = {
    borderBottom: '2px solid #aaa',
    backgroundColor: '#f7f7f7',
    fontWeight: 'bold',
};
const STYLE_TOP_RIGHT_GRID = {
    borderBottom: '2px solid #aaa',
    backgroundColor: '#f7f7f7',
    fontWeight: 'bold',
};

export default class ACEGrid extends Component {
	constructor(props) {
    	super(props);
        this.state = {
            prevSetData: [],
            currSetData: [],
            nextSetData: []
        }
      
        this.timer = null;
        this._getGridData = this._getGridData.bind(this);
        this._cellRenderer = this._cellRenderer.bind(this);
        this._onSectionRendered = this._onSectionRendered.bind(this);
    }

    componentWillMount(){
        let startIndexx = 1;
        let stopIndexx = 1000;
        axios.get(`http://localhost:3001/getStoreReport?startIndex=${startIndexx}&stopIndex=${stopIndexx}`)
        .then((r) => {
            // console.log("r", r);
            this.setState({currSetData: r.data})
        })
        .catch((e) => console.log("e", e))
    }

    _getGridData(){
        const {prevSetData, currSetData, nextSetData} = this.state;
        return prevSetData.concat(currSetData).concat(nextSetData);
    }

    _onSectionRendered ({ columnStartIndex, columnStopIndex, rowStartIndex, rowStopIndex }) {
        // console.log("_onSectionRendered: ", columnStartIndex, columnStopIndex, rowStartIndex, rowStopIndex);
        let newSetIndex = Math.round(rowStopIndex/1000)*1000;
        const data = this._getGridData();
        let startIndex = newSetIndex + 1;
        let stopIndex = newSetIndex + 1000;
        let needNewSetIndex = data.findIndex(e => startIndex === e.rowIndex);
        if (needNewSetIndex === -1){
        
            axios.get(`http://localhost:3001/getStoreReport?startIndex=${startIndex}&stopIndex=${stopIndex}`)
            .then((r) => {
                // console.log("r", r);
                // this.setState({nextSetData: r.data});
                this.setState(
                    {currSetData: this.state.currSetData.concat(r.data)},
                    () => {
                        this._grid.forceUpdateGrids();
                    }
                )
            })
            .catch((e) => console.log("e", e))
        }        
    }

	_cellRenderer({ columnIndex, key, rowIndex, style }) {
        const data = this._getGridData();
        const row = data.find(e => e.rowIndex === rowIndex);
        return (
            <div key={key} style={style} className="Cell">
                {row && row.value && row.value[columnIndex]}
            </div>
        );
    }
 
  	render() {        
        const data = this._getGridData();
        
        return (
            <div>
                {!!data.length ?
                    <div id="ACEGrid" style={{margin: '20px'}}>
                        <AutoSizer disableHeight>
                            {({width}) => (
                                <MultiGrid
                                    ref={(ref) => this._grid = ref}
                                    onSectionRendered={this._onSectionRendered}
                                    cellRenderer={this._cellRenderer}
                                    columnWidth={100}
                                    columnCount={15}
                                    height={600}
                                    rowHeight={30}
                                    rowCount={data.length}
                                    width={width}
                                    fixedColumnCount={0}
                                    fixedRowCount={1}
                                    hideTopRightGridScrollbar
                                    hideBottomLeftGridScrollbar
                                    styleTopLeftGrid={STYLE_TOP_LEFT_GRID}
                                    styleTopRightGrid={STYLE_TOP_RIGHT_GRID}
                                /> 
                            )}
                        </AutoSizer>                        
                    </div>
                :null}
            </div>
		);
  	}
}