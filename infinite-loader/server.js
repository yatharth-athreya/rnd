require('dotenv').config({path: process.env.ENV_VAR_PATH});

const express = require('express');
const app = express();

app.use(express.static('./'));
app.use(express.static('dist'));

// Handle Pre-flight request with CORS
app.options("*", function (req, res, next) {
  corsOptions(res);
  res.statusCode = 200;
  res.setHeader('Content-Length', '0');
  res.end();
})

// CORS config
app.use(function (req, res, next) {
  corsOptions(res);
  next();
});

function corsOptions(res) {
  res.setHeader("Access-Control-Allow-Origin", '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, X-Correlation-Id, Content-Type, delivery-date, Authorization, From");
}

app.get('/health', function (req, res) {
  res.json({ status: "UP" });
});

app.get('/metrics', function(req, res) {
  res.end(prometheus.getMetrics());
});

app.get('/getStoreReport', function (req, res) {
  const {startIndex, stopIndex} = req.query;
  const storeReport = Number(startIndex) === 1 ? [{"rowIndex": 0, "value":["H1", "H2", "H3", "H4", "H5", "H6", "H7", "H8", "H9", "H10", "H11", "H12", "H13", "H14", "H15"]}] : [];
  for (let i = Number(startIndex); i <= Number(stopIndex); i++) {
    storeReport.push({"rowIndex": i, "value":[i + 0.001, i + 0.002, i + 0.003, i + 0.004, i + 0.005, i + 0.006, i + 0.007, i + 0.008, i + 0.009, i + 0.010, i + 0.011, i + 0.012, i + 0.013, i + 0.014, i + 0.015]});
  }
  res.send(storeReport)
})

app.use( function(req, res, next) {
  if (req.originalUrl && req.originalUrl.split("/").pop() === 'favicon.ico') {
    return res.sendStatus(204);
  }
  return next();
});

const port = 3001;

app.listen(port, () => {
  console.log('app listening on', port);
});