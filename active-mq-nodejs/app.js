process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
const stompit = require('stompit');
var fs = require('fs');
const connectOptions = {
  // 'host': 'localhost',
  'host': '10.186.19.102',
  // 'port': 61616,
  'port': 47500,
  ssl:true,
  // key: [ fs.readFileSync('/Users/tkmaau0/git_repositories/PSIN_RnD/python-kafka/kohlsCA.pem','utf8') ],
  ca: [ fs.readFileSync('/Users/tkmaau0/Downloads/ocp-master.gcpusc1-c.lle.xpaas.kohls.com(1).cer','utf8') ],
  // cert:"/Users/tkmaau0/git_repositories/PSIN_RnD/python-kafka/kohlsCA.pem",
  'connectHeaders':{
    'host': '/',
    'login': 'someuser',
    'passcode': 'somepassword',
    'heart-beat': '5000,5000'
  }
};

stompit.connect(connectOptions, function(error, client) {
  
  if (error) {
    console.log('connect error ~ ' + error.message);
    return;
  }
  
  const sendHeaders = {
    'destination': '/queue/test',
    'content-type': 'text/plain'
  };
  
  const frame = client.send(sendHeaders);
  frame.write('hello');
  frame.end();
  
  const subscribeHeaders = {
    'destination': '/queue/test',
    'ack': 'client-individual'
  };
  
  client.subscribe(subscribeHeaders, function(error, message) {
    
    if (error) {
      console.log('subscribe error ' + error.message);
      return;
    }
    
    message.readString('utf-8', function(error, body) {
      
      if (error) {
        console.log('read message error ' + error.message);
        return;
      }
      
      console.log('received message: ' + body);
      
      client.ack(message);
      
      client.disconnect();
    });
  });
});