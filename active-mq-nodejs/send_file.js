process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
var stompit = require('stompit');
var fs = require('fs');
var path = require('path');

var connectParams = {
    host: '10.186.19.102',
    'port': 47500,
    ssl:true,
  // key: [ fs.readFileSync('/Users/tkmaau0/git_repositories/PSIN_RnD/python-kafka/kohlsCA.pem','utf8') ],
  //  ca: [ fs.readFileSync('/Users/tkmaau0/Downloads/ocp-master.gcpusc1-c.lle.xpaas.kohls.com(1).cer','utf8') ],
    connectHeaders:{
        host: '10.186.19.102',
        login: 'someuser',
        passcode: 'somepassword'
    }
};

stompit.connect(connectParams, function(error, client){
    
    if(error){
        console.log('Unable to connect: ' + error.message);
        return;
    }
    
    var filename = path.dirname(module.filename) + '/data/test.dat';
    
    var fileStat = fs.statSync(filename);
    var contentLength = fileStat.size;
    
    var sendParams = {
        'destination': '/queue/test',
        // 'content-type': 'image/jpeg',
        'content-length': contentLength
    };
    
    var frame = client.send(sendParams);
    
    var file = fs.createReadStream(filename);
    file.pipe(frame);
    
    client.disconnect(function(error){
        if(error){
            console.log('Error while disconnecting: ' + error.message);
            return;
        }
        console.log('Sent file and size was '+contentLength);
    });
});
