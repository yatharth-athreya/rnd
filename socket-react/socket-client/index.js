const express = require("express");
const http = require("http");
//const socketIo = require("socket.io");

//Port from environment variable or default - 4001
const port = process.env.PORT || 4003;

//Setting up express and adding socketIo middleware
const app = express();
const server = http.createServer(app);
//const io = socketIo(server);

let socket = require('socket.io-client')('http://127.0.0.1:4001/mynamespace');


//starting speed at 0
var speed = 0;
app.get('/',function(req,res) {
//Simulating reading data every 100 milliseconds

    //some sudo-randomness to change the values but not to drastically
    let nextMin = (speed-2)>0 ? speed-2 : 2;
    let nextMax = speed+5 < 140 ? speed+5 : Math.random() * (130 - 5 + 1) + 5;
    speed = Math.floor(Math.random() * (nextMax - nextMin + 1) + nextMin);

    //we emit the data. No need to JSON serialization!
    
    // socket.on('connect', function () {

    //     socket.emit('incoming data', speed);
    //   });
    console.log('Speed emited  '+ speed);
    socket.emit('incoming data', speed);

res.send("hello");

});

app.listen(4003, function () {
    console.log('Example app listening on port 4003!');
    });