import { combineReducers } from "redux";
import { loadingBarReducer } from 'react-redux-loading-bar';
import {reducer as notifications} from 'react-notification-system-redux';
import dlaState from "./dla-reducer";


export default combineReducers({
  dlaState,
  loadingBar: loadingBarReducer,
  notifications
})
