import React, { Component } from 'react';
import { connect } from 'react-redux';
import { AutoSizer, MultiGrid } from 'react-virtualized';
import {Input} from 'reactstrap';
import './DLAGrid.scss';

class DLAGrid extends Component {
	constructor(props) {
    	super(props);
        this.state = {}
      
        this._gridHeight = this._gridHeight.bind(this);
        this._getGridData = this._getGridData.bind(this);
        this._cellRenderer = this._cellRenderer.bind(this);
    }
    
    _gridHeight (length) {
        const height = length * 30;
        const innerHeight = 200;
        return Math.min(height, innerHeight);
    }

    _getGridData(){
        const {dlaResults} = this.props;
        const data = dlaResults.map((e,i) => {
            return {
                "rowIndex": i,
                "value": [`${e.id}`, `${e.application}`, `${e.department}`, `${e.effStartDate}`, `${e.effEndDate}`]
            }
        });
        return data;
    }

	_cellRenderer({ columnIndex, key, rowIndex, style }) {
        const data = this._getGridData();
        const row = data.find(e => e.rowIndex === rowIndex);
        const rowClassName = rowIndex === 0 ? "cell-header" : "cell-body";
        const cellValue = columnIndex === 0 ? <Input type="checkbox" /> : (row && row.value && row.value[columnIndex-1]);
        return (
            <div key={key} style={style} className={`${rowClassName}`}>
                {cellValue}
            </div>
        );
    }
 
  	render() {        
        const data = this._getGridData();
        
        return (
            <div>
                {!!data.length ?
                    <div id="DLAGrid">
                        <AutoSizer disableHeight>
                            {({width}) => (
                                <MultiGrid
                                    ref={(ref) => this._grid = ref}
                                    cellRenderer={this._cellRenderer}
                                    columnWidth={150}
                                    columnCount={6}
                                    height={this._gridHeight(data.length) + 15}
                                    rowHeight={30}
                                    rowCount={data.length}
                                    width={width}
                                    fixedColumnCount={0}
                                    fixedRowCount={1}
                                    hideTopRightGridScrollbar
                                    hideBottomLeftGridScrollbar
                                /> 
                            )}
                        </AutoSizer>                        
                    </div>
                :null}
            </div>
		);
  	}
}

DLAGrid.propTypes = {}

const mapStateToProps = state => {
    return {
        dlaResults: state.dlaState.dlaResults
    }
}

export default connect(mapStateToProps)(DLAGrid);