import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import 'react-select/dist/react-select.css';
import 'react-virtualized-select/styles.css';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducers/index';
import { createLogger } from 'redux-logger';
import promise from 'redux-promise-middleware';
import { loadingBarMiddleware } from 'react-redux-loading-bar';
import DAAdmin from './containers/Admin/DepartmentLevelAccess/DepartmentLevelAccess';
import 'react-dates/lib/css/_datepicker.css'; 


let middleware = [promise(), thunk, loadingBarMiddleware({
  promiseTypeSuffixes: ['START', 'SUCCESS', 'ERROR'],
})];

if (process.env.NODE_ENV !== 'production') {
  middleware.push(createLogger());
}

const createStoreWithMiddleware = applyMiddleware(...middleware)(createStore);
const store = createStoreWithMiddleware(reducers);


class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <Provider store={store}>
        <Switch>
          <Route path="/departmentlevelaccessadmin" render={() => <DAAdmin userDetails={this.props.userDetails}/>} />
        </Switch>
        </Provider>
    )
  }
}

export default App;
