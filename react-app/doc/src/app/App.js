import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import 'react-select/dist/react-select.css';
import 'react-virtualized-select/styles.css';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducers/index';
import { createLogger } from 'redux-logger';
import promise from 'redux-promise-middleware';
import { loadingBarMiddleware } from 'react-redux-loading-bar';
import AssetRecovery from './containers/Document/AssetRecovery';
import NewDocument from './containers/Document/NewDocument';
import SearchDocument from './containers/Document/SearchDocument';
import 'react-dates/lib/css/_datepicker.css'; 


let middleware = [promise(), thunk, loadingBarMiddleware({
  promiseTypeSuffixes: ['START', 'SUCCESS', 'ERROR'],
})];

if (process.env.NODE_ENV !== 'production') {
  middleware.push(createLogger());
}

const createStoreWithMiddleware = applyMiddleware(...middleware)(createStore);
const store = createStoreWithMiddleware(reducers);


class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <Provider store={store}>
        <Switch>
          <Route path="/assetrecoverydocument" render={() => <AssetRecovery userDetails={this.props.userDetails}/>} />
          <Route path="/newdocument" render={() => <NewDocument userDetails={this.props.userDetails}/>} />
          <Route path="/searchdocument" render={() => <SearchDocument userDetails={this.props.userDetails}/>} />
        </Switch>
        </Provider>
    )
  }
}

export default App;
