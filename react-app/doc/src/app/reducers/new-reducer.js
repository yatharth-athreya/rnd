import moment from "moment";
let today = new Date();
today = moment(today);

const initialState = {
    numberOfDays: "",
    netCost: NaN,
    markup: NaN,
    defaultMarkup: NaN,
    retail: NaN,
    poNumber: NaN,
    avlCostOptions: [],
    avlCost: NaN,
    fiscalMonth: "",
    defaultFiscalMonth: "",
    vendorEmail: "",
    defaultVendorEmail: "",
    effectiveDate: today,
    fetchURL: "",
    showNotesModal: false,
    showBrandModal: false,
    showPrintConfirmationModal: false,
    showPrintReportsModal: false,
    daCreateValidationError: false,
    showExitConfirmationModal: false,
    debitAllowanceValue: "", 
    priceChangeDocumentValue: "",
    debitTypeOptions: [],
    debitTypeSelected: [],
    vendorOptions: [],
    vendorSelected: [],
    statusOptions: [{codeValue: "10", codeDescription: "Worksheet (new)", isChecked: true}],
    statusSelected: [{codeValue: "10", codeDescription: "Worksheet (new)"}],
    collectionMethodOptions: [],
    collectionMethodSelected: [],
    reasonOptions: [],
    reasonAvlOptions: [],
    reasonSelected: [],
    newScreenFields : {
        showCollectionMethod: false,
        showNumberOfDays: false,
        showReason: false,
        showVendorEmail: false,
        showMarkup: false,
        showRetail: false,
        showAvailableCost: false
    },
    departmentOptions: [],
    departmentSelected: [],
    subclassOptions: [],
    subclassSelected: [],
    noteTypeSelected: [{label: "Credit Auth Number", value: "Credit Auth Number"}],
    noteTypeOptions: [{label: "Credit Auth Number", value: "Credit Auth Number", isChecked: true}, {label: "AP Notes", value: "AP Notes", isChecked: false}],
    noteText: "",
    newDAStatus: "",
    newDAData: [],
    brandDetails: [
        {
            "rowIndex": 0,
            "value":["Brand Lbl Seq", "Brand Desc", "Proration %", "Net Cost"]
        },
        {
            "rowIndex": 1, 
            "value": ["1", "a", 25, 25]
        },
        {
            "rowIndex": 2, 
            "value": ["2", "b", 25, 25]
        },
        {
            "rowIndex": 3, 
            "value": ["3", "c", 25, 25]
        },
        {
            "rowIndex": 4, 
            "value": ["4", "d", 25, 25]
        }
    ],
    brandDetailsProrationFlag: false,
    notesErrorFlag: false
}

export default (state= initialState, action) => {
    switch(action.type) {
        case "DA_SET_SERVICE_URL":
            return {
                ...state,
                fetchURL: action.fetchURL
            }
        case "DA_NEW_CHANGE_EFF_DATE":
            return {
                ...state,
                effectiveDate: action.effectiveDate,
                fiscalMonth: ""
            }
        case "DA_NEW_SET_DEBIT_TYPE_FS":
            return {
                ...state
            }
        case "DA_NEW_SET_DEBIT_TYPE":
            return {
                ...state,
                debitTypeOptions: action.data.filter(e => ["Advertising Allowance", "Ad Coop from Vendor Funding", "Markdown Allowance", "Mkdn Alw Cost Only", "Mark out of Stock", "Mkdn Alw Profit Asst - Cost", "Mkdn Alw Profit Asst - Cst/Rtl"].includes(e.codeDescription))
            }
        case "DA_NEW_SET_VENDOR_FS":
            return {
                ...state
            }
        case "DA_NEW_SET_VENDOR":
            return {
                ...state,
                vendorOptions: action.data
            }
        case "DA_NEW_SET_FISCAL_MONTH_FS":
            return {
                ...state
            }
        case "DA_NEW_SET_FISCAL_MONTH":
            return {
                ...state,
                fiscalMonth: action.data,
                defaultFiscalMonth: (state.defaultFiscalMonth || action.data)
            }
        case "DA_NEW_SET_COLLECTION_MONTH_FS":
            return {
                ...state
            }
        case "DA_NEW_SET_COLLECTION_MONTH":
            return {
                ...state,
                collectionMethodOptions: action.data
            }
        case "DA_NEW_SET_REASON_FS":
            return {
                ...state
            }
        case "DA_NEW_SET_REASON":
            return {
                ...state,
                reasonOptions: action.data
            }
        case "DA_NEW_SET_VENDOR_DEPARTMENT_FS":
            return {
            ...state
            }
        case "DA_NEW_SET_VENDOR_DEPARTMENT":
            return {
                ...state,
                departmentOptions: !!action.data.length ? action.data.map(val => ({...val, department: val.departmentNumber + " - " + val.departmentName})) : []
            }
        case "DA_NEW_SET_VENDOR_EMAIL_ADDRESS_FS":
            return {
                ...state
            }
        case "DA_NEW_SET_VENDOR_EMAIL_ADDRESS":
            return {
                ...state,
                vendorEmail: action.data.emailAddress,
                defaultVendorEmail: action.data.emailAddress
            }
        case "DA_NEW_SET_SUB_CLASS_FS":
            return {
                ...state
            }
        case "DA_NEW_SET_SUB_CLASS":
            return {
                ...state,
                subclassOptions: !!action.data.length ? action.data.map(val => ({...val, subclass: val.subclassNumber + " - " + val.subclassName})) : []
            }
        case "DA_NEW_SET_AVAILABLE_COST_FS":
            return {
            ...state
            }
        case "DA_NEW_SET_AVAILABLE_COST":
            return {
                ...state,
                avlCostOptions: action.data,
                avlCost: action.data.totalAvailableCostAmount
            }
        case "DA_NEW_SAVE_DEBIT_ALLOWANCE_FS":
            return {
                ...state,
                newDAStatus: action.status
            }
        case "DA_NEW_SAVE_DEBIT_ALLOWANCE":
            return {
                ...state,
                newDAData: action.data,
                newDAStatus: "success"
            }
        case "DA_NEW_GET_NOTE_TYPE_FS":
            return {
                ...state
            }
        case "DA_NEW_GET_NOTE_TYPE":
            return {
                ...state
            }
        case "DA_NEW_SAVE_NOTE_TYPE_FS":
            return {
                ...state
            }
        case "DA_NEW_SAVE_NOTE_TYPE":
            return {
                ...state
            }
        case "DA_NEW_CHECK_BRAND_FOR_DEPARTMENT_FS":
            return {
                ...state
            }
        case "DA_NEW_CHECK_BRAND_FOR_DEPARTMENT":
            return {
                ...state
            }
        case "DA_NEW_SAVE_BRANDS_FS":
            return {
                ...state
            }
        case "DA_NEW_SAVE_BRANDS":
            return {
                ...state
            }
        case "DA_NEW_GET_NEXT_AP_DOCUMENT_NUMBER_FS":
            return {
                ...state
            }
        case "DA_NEW_GET_NEXT_AP_DOCUMENT_NUMBER":
            return {
                ...state
            }
        case "DA_NEW_SET_DEFAULT_MARKUP_PERCENTAGE_FS":
            return {
                ...state
            }
        case "DA_NEW_SET_DEFAULT_MARKUP_PERCENTAGE":
            return {
                ...state
            }
        case "DA_NEW_TOGGLE_EXIT":
            return {
                ...state,
                showExitConfirmationModal: !state.showExitConfirmationModal,
                newDAStatus: "",
                newDAData: []
            }
        case "DA_NEW_TOGGLE_NOTES":
            return {
                ...state,
                showNotesModal: !state.showNotesModal
            }
        case "DA_NEW_TOGGLE_BRAND":
            return {
                ...state,
                showBrandModal: !state.showBrandModal
            }
        case "DA_NEW_TOGGLE_CREATE_NEW_ERROR_MODAL":
            return {
                ...state,
                daCreateValidationError: !state.daCreateValidationError
            }
        case "DA_NEW_TOGGLE_PRINT_CONFIRMATION_MODAL":
            return {
                ...state,
                showPrintConfirmationModal: false
            }
        case "DA_NEW_TOGGLE_PRINT_REPORTS_MODAL":
            return {
                ...state,
                showPrintConfirmationModal: false,
                showPrintReportsModal: action.bool
            }
        case "DA_NEW_REFRESH":
            return {
                ...state,
                debitTypeSelected: [],
                debitTypeOptions: state.debitTypeOptions.map(val => ({ ...val, isChecked: false })),
                vendorSelected: [],
                vendorOptions: state.vendorOptions.map(val => ({ ...val, isChecked: false })),
                effectiveDate: today,
                collectionMethodSelected: [],
                collectionMethodOptions: state.collectionMethodOptions.map(val => ({ ...val, isChecked: false })),
                numberOfDays: "",
                netCost: NaN,
                markup: state.defaultMarkup,
                retail: NaN,
                poNumber: NaN,
                avlCost: NaN,
                avlCostOptions: [],
                fiscalMonth: state.defaultFiscalMonth,
                vendorEmail: state.defaultVendorEmail,
                reasonSelected: [],
                reasonAvlOptions: [],
                departmentSelected: [],
                departmentOptions: [],
                subclassSelected: [],
                subclassOptions: [],
                noteTypeSelected: [{label: "Credit Auth Number", value: "Credit Auth Number"}],
                noteTypeOptions: [{label: "Credit Auth Number", value: "Credit Auth Number", isChecked: true}, {label: "AP Notes", value: "AP Notes", isChecked: false}],
                noteText: ""
            }
        case "DA_NEW_CHANGE_DEBIT_TYPE":
            let setReasonAvlOptions = action.reasonAvlOptions;
            if (!!setReasonAvlOptions.length) {
                setReasonAvlOptions = setReasonAvlOptions.map((e, i) => ({...e, isChecked: i === 0}));
            }
            return {
                ...state,
                numberOfDays: "",
                netCost: NaN,
                markup: state.defaultMarkup,
                retail: NaN,
                poNumber: NaN,
                reasonSelected: !!setReasonAvlOptions.length ? [setReasonAvlOptions[0]] : [],
                reasonAvlOptions: setReasonAvlOptions,
                newScreenFields: action.newScreenFields,
                debitTypeSelected: state.debitTypeOptions.filter(val => action.data.codeValue == val.codeValue ? !!action.data.isChecked : false),
                debitTypeOptions: state.debitTypeOptions.map(val => val.codeValue === action.data.codeValue ? action.data : { ...val, isChecked: false})
            }
        case "DA_NEW_CHANGE_VENDOR":
            return {
                ...state,
                avlCost: NaN,
                avlCostOptions: [],
                vendorEmail: "",
                departmentSelected: [],
                departmentOptions: [],
                subclassSelected: [],
                subclassOptions: [],
                vendorSelected: state.vendorOptions.filter(val => action.data.id == val.id ? !!action.data.isChecked : false),
                vendorOptions: state.vendorOptions.map(val => val.id === action.data.id ? action.data : { ...val, isChecked: false})
            }
        case "DA_NEW_CHANGE_STATUS":
            return {
                ...state,
                statusSelected: state.statusOptions.filter(val => action.data.id == val.id ? !!action.data.isChecked : false),
                statusOptions: state.statusOptions.map(val => val.id === action.data.id ? action.data : { ...val, isChecked: false})
            }
        case "DA_NEW_CHANGE_COLLECTION_METHOD":
            return {
                ...state,
                collectionMethodSelected: state.collectionMethodOptions.filter(val => action.data.codeValue == val.codeValue ? !!action.data.isChecked : false),
                collectionMethodOptions: state.collectionMethodOptions.map(val => val.codeValue === action.data.codeValue ? action.data : { ...val, isChecked: false}),
                numberOfDays: !!action.data.isChecked && ["02", "03"].includes(action.data.codeValue) ? "30" : ""
            }
        case "DA_NEW_CHANGE_NUMBER_OF_DAYS":
            return {
                ...state,
                numberOfDays: action.data
            }
        case "DA_NEW_CHANGE_REASONS":
            return {
                ...state,
                reasonSelected: state.reasonAvlOptions.filter(val => action.data.codeValue == val.codeValue ? !!action.data.isChecked : false),
                reasonAvlOptions: state.reasonAvlOptions.map(val => val.codeValue === action.data.codeValue ? action.data : { ...val, isChecked: false})
            }
        case "DA_NEW_CHANGE_DEPARTMENT":
            return {
                ...state,
                departmentSelected: state.departmentOptions.filter(val => action.data.department == val.department ? !!action.data.isChecked : false),
                departmentOptions: state.departmentOptions.map(val => val.department === action.data.department ? action.data : { ...val, isChecked: false}),
                subclassSelected: [],
                subclassOptions: [],
                avlCost: NaN,
                avlCostOptions: [],
                vendorEmail: ""
            }
        case "DA_NEW_CHANGE_SUBCLASS":
            return {
                ...state,
                subclassSelected: state.subclassOptions.filter(val => action.data.subclass == val.subclass ? !!action.data.isChecked : false),
                subclassOptions: state.subclassOptions.map(val => val.subclass === action.data.subclass ? action.data : { ...val, isChecked: false})
            }
        case "DA_NEW_CHANGE_NET_COST":
            return {
                ...state,
                netCost: action.data
            }
        case "DA_NEW_CHANGE_MARKUP":
            return {
                ...state,
                markup: action.data,
                defaultMarkup: action.data
            }
        case "DA_NEW_CHANGE_RETAIL":
            return {
                ...state,
                retail: action.data
            }
        case "DA_NEW_CHANGE_AVAILABLE_COST":
            return {
                ...state,
                avlCost: action.data
            }
        case "DA_NEW_CHANGE_PO_NUMBER":
            return {
                ...state,
                poNumber: action.data
            }
        case "DA_NEW_CHANGE_NOTE_TYPE":
            return {
                ...state,
                noteTypeSelected: state.noteTypeOptions.filter(val => action.data.value == val.value ? !!action.data.isChecked : false),
                noteTypeOptions: state.noteTypeOptions.map(val => val.value === action.data.value ? action.data : { ...val, isChecked: false})
            }
        case "DA_NEW_CHANGE_NOTE_TEXT":
            return {
                ...state,
                noteText: action.data
            }
        case "DA_NEW_REFRESH_NOTES":
            return {
                ...state,
                noteTypeSelected: [{label: "Credit Auth Number", value: "Credit Auth Number"}],
                noteTypeOptions: [{label: "Credit Auth Number", value: "Credit Auth Number", isChecked: true}, {label: "AP Notes", value: "AP Notes", isChecked: false}],
                noteText: "",
                notesErrorFlag: false
            }
        case "DA_NEW_TOGGLE_CREATION_STATUS":
            return {
                ...state,
                newDAStatus: ""
            }
        case "DA_NEW_CHANGE_BRAND_DETAILS":
            return {
                ...state,
                brandDetails: action.data
            }
        case "DA_NEW_BRAND_DETAILS_FLAG":
            return {
                ...state,
                brandDetailsProrationFlag: action.brandDetailsProrationFlag
            }
        case "DA_NEW_NOTES_ERROR_FLAG":
            return {
                ...state,
                notesErrorFlag: action.notesErrorFlag
            }
        default:
            return state;
    }
}