import { combineReducers } from "redux";
import { loadingBarReducer } from 'react-redux-loading-bar';
import {reducer as notifications} from 'react-notification-system-redux';
import searchState from "./search-reducer";
import newState from "./new-reducer";


export default combineReducers({
  newState,
  searchState,
  loadingBar: loadingBarReducer,
  notifications
})
