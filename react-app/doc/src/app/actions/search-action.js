import axios from "axios";

export const selectType = (selectedType) => {
    return (dispatch, getState) => {
        dispatch({ type: "DASEARCH-SET-SEARCH-TYPE", selectedType });
    }
}