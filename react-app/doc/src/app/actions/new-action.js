import axios from "axios";
import moment from "moment";

export const onChangeEffectiveDate = (date) => {
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_CHANGE_EFF_DATE", effectiveDate: date });
        dispatch(getFiscalMonth());
    }
}

export const takeMeHome = () => {
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_REFRESH" });
        dispatch({ type: "DA_NEW_TOGGLE_EXIT" });
        document.querySelector(".fa-home").click();
    }
}

export const toggleExitConfirmationModal = () => {
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_TOGGLE_EXIT" });
    }
}

export const toggleNotes = () => {
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_TOGGLE_NOTES" });
        dispatch(refreshNotes());
        // dispatch(getNoteType());
    }
}

export const toggleBrand = () => {
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_TOGGLE_BRAND" });
    }
}

export const togglePrintConfirmationModal = () => {
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_TOGGLE_PRINT_CONFIRMATION_MODAL" });
    }
}

export const togglePrintReportsModal = (bool) => {
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_TOGGLE_PRINT_REPORTS_MODAL", bool });
    }
}

export const validateDACreation = () => {
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_TOGGLE_CREATE_NEW_ERROR_MODAL" });
    }
}

export const fetchCreateDAFormData = () => {
    return (dispatch, getState) => {
        // dispatch(getJWToken());

        axios.get('/epsvg')
        .then(udr => {
            dispatch({type: "DA_SET_SERVICE_URL", fetchURL: udr.data.DAS_SERVICE_URL});
            dispatch(getDebitType());
            dispatch(getStatus());
            dispatch(getVendor());
            dispatch(getFiscalMonth());
            dispatch(getCollectionMethod());
            dispatch(getReason());
        })
        .catch()
    }
}

export const getJWToken = () => {
    return (dispatch, getState) => {
        axios.get('/ausvg')
        .then( res => {
            axios.defaults.headers.common['Authorization'] = res.data['Authorization'] ? res.data['Authorization'] : '';
            axios.defaults.headers.common['Delivery-Date'] = res.data['Delivery-Date'] ? res.data['Delivery-Date'] : '';
            axios.defaults.headers.common['From'] = res.data['From'] ? res.data['From'] : '';
            axios.defaults.headers.common['X-Correlation-ID'] = res.data['X-Correlation-ID'] ? res.data['X-Correlation-ID'] : '';
        })
        .catch()
        setTimeout(() => {dispatch(getJWToken())}, 24 * 60 * 60 * 1000);
    }
}

export const getDebitType = () => {
    return (dispatch, getState) => {
        const {fetchURL} = getState().newState;

        dispatch({type: "DA_NEW_SET_DEBIT_TYPE_FS", status: "Loading"});
        axios.get(`${fetchURL}/v1/debit-types`)
        .then( res => {
            if (res.status === 200) {
                dispatch({type: "DA_NEW_SET_DEBIT_TYPE", data: res.data.result})
            } else {
                dispatch({type: "DA_NEW_SET_DEBIT_TYPE_FS", status: "Error"})
            }
        })
        .catch(() => {
            dispatch({type: "DA_NEW_SET_DEBIT_TYPE_FS", status: "Error"})
        })
    }
}

export const getStatus = () => {
    return (dispatch, getState) => {
        const {fetchURL} = getState().newState;

        dispatch({type: "DA_NEW_SET_STATUS_FS", status: "Loading"});
        axios.get(`${fetchURL}/v1/debit-allowance-status`)
        .then( res => {
            if (res.status === 200) {
                dispatch({type: "DA_NEW_SET_STATUS", data: res.data.result})
            } else {
                dispatch({type: "DA_NEW_SET_STATUS_FS", status: "Error"})
            }
        })
        .catch(() => {
            dispatch({type: "DA_NEW_SET_STATUS_FS", status: "Error"})
        })
    }
}

export const getVendor = () => {
    return (dispatch, getState) => {
        const {fetchURL} = getState().newState;

        dispatch({type: "DA_NEW_SET_VENDOR_FS", status: "Loading"});
        axios.get(`${fetchURL}/v1/vendors`)
        .then( res => {
            if (res.status === 200) {
                dispatch({type: "DA_NEW_SET_VENDOR", data: res.data.result})
            } else {
                dispatch({type: "DA_NEW_SET_VENDOR_FS", status: "Error"})
            }
        })
        .catch(() => {
            dispatch({type: "DA_NEW_SET_VENDOR_FS", status: "Error"})
        })
    }
}

export const getFiscalMonth = () => {
    return (dispatch, getState) => {
        const {fetchURL, effectiveDate} = getState().newState;
        const date = moment(effectiveDate).format("MM/DD/YYYY")

        dispatch({type: "DA_NEW_SET_FISCAL_MONTH_FS", status: "Loading"});
        axios.get(`${fetchURL}/v1/fiscal-month?effectiveDate=${date}`)
        .then( res => {
            if (res.status === 200) {
                dispatch({type: "DA_NEW_SET_FISCAL_MONTH", data: res.data.result})
            } else {
                dispatch({type: "DA_NEW_SET_FISCAL_MONTH_FS", status: "Error"})
            }
        })
        .catch(() => {
            dispatch({type: "DA_NEW_SET_FISCAL_MONTH_FS", status: "Error"})
        })
    }
}

export const getCollectionMethod = () => {
    return (dispatch, getState) => {
        const {fetchURL} = getState().newState;

        dispatch({type: "DA_NEW_SET_COLLECTION_MONTH_FS", status: "Loading"});
        axios.get(`${fetchURL}/v1/collection-methods`)
        .then( res => {
            if (res.status === 200) {
                dispatch({type: "DA_NEW_SET_COLLECTION_MONTH", data: res.data.result})
            } else {
                dispatch({type: "DA_NEW_SET_COLLECTION_MONTH_FS", status: "Error"})
            }
        })
        .catch(() => {
            dispatch({type: "DA_NEW_SET_COLLECTION_MONTH_FS", status: "Error"})
        })
    }
}

export const getReason = () => {
    return (dispatch, getState) => {
        const {fetchURL} = getState().newState;

        dispatch({type: "DA_NEW_SET_REASON_FS", status: "Loading"});
        axios.get(`${fetchURL}/v1/reasons`)
        .then( res => {
            if (res.status === 200) {
                dispatch({type: "DA_NEW_SET_REASON", data: res.data.result})
            } else {
                dispatch({type: "DA_NEW_SET_REASON_FS", status: "Error"})
            }
        })
        .catch(() => {
            dispatch({type: "DA_NEW_SET_REASON_FS", status: "Error"})
        })
    }
}

export const getVendorDepartment = () => {
    return (dispatch, getState) => {
        const {fetchURL, vendorSelected} = getState().newState;
        const vendorId = !!vendorSelected.length && vendorSelected[0].id ? vendorSelected[0].id : "207287";

        dispatch({type: "DA_NEW_SET_VENDOR_DEPARTMENT_FS", status: "Loading"});
        axios.get(`${fetchURL}/v1/vendor-department?vendorId=${vendorId}`)
        .then( res => {
            if (res.status === 200) {
                dispatch({type: "DA_NEW_SET_VENDOR_DEPARTMENT", data: res.data.result})
            } else {
                dispatch({type: "DA_NEW_SET_VENDOR_DEPARTMENT_FS", status: "Error"})
            }
        })
        .catch(() => {
            dispatch({type: "DA_NEW_SET_VENDOR_DEPARTMENT_FS", status: "Error"})
        })
    }
}

export const getVendorEmailAddress = () => {
    return (dispatch, getState) => {
        const {fetchURL, vendorSelected, departmentSelected} = getState().newState;
        const vendorId = !!vendorSelected.length && vendorSelected[0].id ? vendorSelected[0].id : "207287";
        const departmentId = !!departmentSelected.length && departmentSelected[0].departmentNumber ? departmentSelected[0].departmentNumber : "950";

        dispatch({type: "DA_NEW_SET_VENDOR_EMAIL_ADDRESS_FS", status: "Loading"});
        axios.get(`${fetchURL}/v1/vendor-email-address?vendorId=${vendorId}&departmentId=${departmentId}`)
        .then( res => {
            if (res.status === 200) {
                dispatch({type: "DA_NEW_SET_VENDOR_EMAIL_ADDRESS", data: res.data.result})
            } else {
                dispatch({type: "DA_NEW_SET_VENDOR_EMAIL_ADDRESS_FS", status: "Error"})
            }
        })
        .catch(() => {
            dispatch({type: "DA_NEW_SET_VENDOR_EMAIL_ADDRESS_FS", status: "Error"})
        })
    }
}

export const getSubClass = () => {
    return (dispatch, getState) => {
        const {fetchURL, departmentSelected} = getState().newState;
        const departmentId = !!departmentSelected.length && departmentSelected[0].departmentNumber ? departmentSelected[0].departmentNumber : "234";

        dispatch({type: "DA_NEW_SET_SUB_CLASS_FS", status: "Loading"});
        axios.get(`${fetchURL}/v1/department-subclass?departmentId=${departmentId}`)
        .then( res => {
            if (res.status === 200) {
                dispatch({type: "DA_NEW_SET_SUB_CLASS", data: res.data.result})
            } else {
                dispatch({type: "DA_NEW_SET_SUB_CLASS_FS", status: "Error"})
            }
        })
        .catch(() => {
            dispatch({type: "DA_NEW_SET_SUB_CLASS_FS", status: "Error"})
        })
    }
}

export const getAvailableCost = () => {
    return (dispatch, getState) => {
        const {fetchURL, vendorSelected, departmentSelected, effectiveDate} = getState().newState;
        const vendorId = !!vendorSelected.length && vendorSelected[0].id ? vendorSelected[0].id : "10964";
        const departmentId = !!departmentSelected.length && departmentSelected[0].departmentNumber ? departmentSelected[0].departmentNumber : "60";
        const date = moment(effectiveDate).format("MM/DD/YYYY");

        dispatch({type: "DA_NEW_SET_AVAILABLE_COST_FS", status: "Loading"});
        axios.get(`${fetchURL}/v1/available-cost?vendorId=${vendorId}&departmentId=${departmentId}&effectiveDate=${date}`)
        .then( res => {
            if (res.status === 200) {
                dispatch({type: "DA_NEW_SET_AVAILABLE_COST", data: res.data.result})
            } else {
                dispatch({type: "DA_NEW_SET_AVAILABLE_COST_FS", status: "Error"})
            }
        })
        .catch(() => {
            dispatch({type: "DA_NEW_SET_AVAILABLE_COST_FS", status: "Error"})
        })
    }
}

export const saveDebitAllowance = () => {
    return (dispatch, getState) => {
        const {fetchURL, newScreenFields, avlCost, collectionMethodSelected, debitTypeSelected, departmentSelected, effectiveDate, vendorEmail, fiscalMonth, markup, netCost, numberOfDays, poNumber, reasonAvlOptions, retail, subclassSelected, vendorSelected } = getState().newState;
        let postData = {
            "availableCostAmount": (newScreenFields.showAvailableCost && avlCost),
            "collectionMethodCode": (!!collectionMethodSelected.length && collectionMethodSelected[0].codeValue ? collectionMethodSelected[0].codeValue : ""),
            "debitType": (!!debitTypeSelected.length && debitTypeSelected[0].codeValue ? debitTypeSelected[0].codeValue : ""),
            "departmentNumber": (!!departmentSelected.length && departmentSelected[0].departmentNumber ? departmentSelected[0].departmentNumber : ""),
            "effectiveDate": (moment(effectiveDate).format("MM/DD/YYYY")),
            "emailAddress": (newScreenFields.showVendorEmail && vendorEmail),
            "fiscalMonth": (fiscalMonth && fiscalMonth.fiscalMonth ? Number(fiscalMonth.fiscalMonth) : ""),
            "fiscalYear": (fiscalMonth && fiscalMonth.fiscalYear ? Number(fiscalMonth.fiscalYear) : ""),
            "markupPercentage": (newScreenFields.showMarkup && markup ? Number(markup) : ""),
            "netCostAmount": (netCost ? Number(netCost) : ""),
            "numberOfDays": (numberOfDays ? Number(numberOfDays) : ""),
            "poNumber": (poNumber),
            "reasonCode": (!!reasonAvlOptions.length && reasonAvlOptions[0].codeValue ? reasonAvlOptions[0].codeValue : ""),
            "retailAmount": (newScreenFields.showRetail && retail ? Number(retail) : ""),
            "statusCode": ("10"),
            "subClassNumber": (!!subclassSelected.length && subclassSelected[0].subclassNumber ? subclassSelected[0].subclassNumber : ""),
            "vendorId": (!!vendorSelected.length && vendorSelected[0].id ? vendorSelected[0].id : "")
        };
        for (let key in postData) {
            if (["", false].includes(postData[key])) {
                delete postData[key]
            }
        }
        if (!postData.debitType || !postData.vendorId || !postData.effectiveDate || !postData.collectionMethodCode || !postData.netCostAmount || !postData.departmentNumber || !postData.subClassNumber || (!postData.emailAddress && newScreenFields.showVendorEmail)){
            return dispatch(validateDACreation());
        }

        dispatch({type: "DA_NEW_SAVE_DEBIT_ALLOWANCE_FS", status: "Loading"});
        axios.post(`${fetchURL}/v1/buyer/save-debit-allowance`, postData)
        .then( res => {
            if (res.status === 200) {
                dispatch({type: "DA_NEW_SAVE_DEBIT_ALLOWANCE", data: res.data.result});
                // dispatch(getCheckBrandForDepartment());
            } else {
                dispatch({type: "DA_NEW_SAVE_DEBIT_ALLOWANCE_FS", status: "Error"});
            }
        })
        .catch(() => {
            dispatch({type: "DA_NEW_SAVE_DEBIT_ALLOWANCE_FS", status: "Error"});
        })
    }
}

export const getNoteType = () => {
    return (dispatch, getState) => {
        const {fetchURL} = getState().newState;

        dispatch({type: "DA_NEW_GET_NOTE_TYPE_FS", status: "Loading"});
        axios.post(`${fetchURL}/getNoteType`)
        .then( res => {
            if (res.status === 200) {
                dispatch({type: "DA_NEW_GET_NOTE_TYPE", data: res.data.result});
            } else {
                dispatch({type: "DA_NEW_GET_NOTE_TYPE_FS", status: "Error"});
            }
        })
        .catch(() => {
            dispatch({type: "DA_NEW_GET_NOTE_TYPE_FS", status: "Error"});
        })
    }
}

export const saveNotes = () => {
    return (dispatch, getState) => {
        const {fetchURL, noteTypeSelected, noteText} = getState().newState;
        const notesErrorFlag = !noteText || !(noteTypeSelected.length && noteTypeSelected[0].value);
        dispatch({type: "DA_NEW_NOTES_ERROR_FLAG", notesErrorFlag});
        if (notesErrorFlag){
            return;
        }

        const postData =  {
            "notesType" : noteTypeSelected[0].value,
            "notesText" : noteText
        };
        dispatch({type: "DA_NEW_SAVE_NOTE_TYPE_FS", status: "Loading"});
        axios.post(`${fetchURL}/saveNotes`, postData)
        .then( res => {
            if (res.status === 200) {
                dispatch({type: "DA_NEW_SAVE_NOTE_TYPE", data: res.data.result});
            } else {
                dispatch({type: "DA_NEW_SAVE_NOTE_TYPE_FS", status: "Error"});
            }
        })
        .catch(() => {
            dispatch({type: "DA_NEW_SAVE_NOTE_TYPE_FS", status: "Error"});
        })
    }
}

export const getCheckBrandForDepartment = () => {
    return (dispatch, getState) => {
        const {fetchURL} = getState().newState;

        dispatch({type: "DA_NEW_CHECK_BRAND_FOR_DEPARTMENT_FS", status: "Loading"});
        axios.get(`${fetchURL}/getCheckBrandForDepartment`)
        .then( res => {
            if (res.status === 200) {
                dispatch({type: "DA_NEW_CHECK_BRAND_FOR_DEPARTMENT", data: res.data.result});
                // dispatch(getNextAPDocumentNumber());
            } else {
                dispatch({type: "DA_NEW_CHECK_BRAND_FOR_DEPARTMENT_FS", status: "Error"});
            }
        })
        .catch(() => {
            dispatch({type: "DA_NEW_CHECK_BRAND_FOR_DEPARTMENT_FS", status: "Error"});
        })
    }
}

export const saveBrands = () => {
    return (dispatch, getState) => {
        const {fetchURL, brandDetails} = getState().newState;
        const brandDetailsProrationFlag = !!brandDetails.length && brandDetails.map((e, i) => i === 0 ? 0 : e.value[2]).reduce((a,b) => a + b, 0) !== 100;
        dispatch({type: "DA_NEW_BRAND_DETAILS_FLAG", brandDetailsProrationFlag});
        if (brandDetailsProrationFlag){
            return;
        }

        const postData = {
            debitAllowanceNumber: "",
            brandDetails
        };
        dispatch({type: "DA_NEW_SAVE_BRANDS_FS", status: "Loading"});
        axios.get(`${fetchURL}/saveBrands`, postData)
        .then( res => {
            if (res.status === 200) {
                dispatch({type: "DA_NEW_SAVE_BRANDS", data: res.data.result})
            } else {
                dispatch({type: "DA_NEW_SAVE_BRANDS_FS", status: "Error"})
            }
        })
        .catch(() => {
            dispatch({type: "DA_NEW_SAVE_BRANDS_FS", status: "Error"})
        })
    }
}

export const getNextAPDocumentNumber = () => {
    return (dispatch, getState) => {
        const {fetchURL} = getState().newState;

        dispatch({type: "DA_NEW_GET_NEXT_AP_DOCUMENT_NUMBER_FS", status: "Loading"});
        axios.get(`${fetchURL}/getNextAPDocumentNumber`)
        .then( res => {
            if (res.status === 200) {
                dispatch({type: "DA_NEW_GET_NEXT_AP_DOCUMENT_NUMBER", data: res.data.result})
            } else {
                dispatch({type: "DA_NEW_GET_NEXT_AP_DOCUMENT_NUMBER_FS", status: "Error"})
            }
        })
        .catch(() => {
            dispatch({type: "DA_NEW_GET_NEXT_AP_DOCUMENT_NUMBER_FS", status: "Error"})
        })
    }
}

export const getDefaultMarkupPercentage = () => {
    return (dispatch, getState) => {
        const {fetchURL} = getState().newState;

        dispatch({type: "DA_NEW_SET_DEFAULT_MARKUP_PERCENTAGE_FS", status: "Loading"});
        axios.get(`${fetchURL}/getDefaultMarkupPercentage`)
        .then( res => {
            if (res.status === 200) {
                dispatch({type: "DA_NEW_SET_DEFAULT_MARKUP_PERCENTAGE", data: res.data.result})
            } else {
                dispatch({type: "DA_NEW_SET_DEFAULT_MARKUP_PERCENTAGE_FS", status: "Error"})
            }
        })
        .catch(() => {
            dispatch({type: "DA_NEW_SET_DEFAULT_MARKUP_PERCENTAGE_FS", status: "Error"})
        })
    }
}

export const refreshNew = (data) => {
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_REFRESH" });
    }
}

export const changeDebitTypeSelected = (data) => {
    const newScreenFields = {
        showCollectionMethod: !!data.isChecked && ["Advertising Allowance", "Mark out of Stock", "Markdown Allowance", "Mkdn Alw Cost Only"].includes(data.codeDescription),
        showNumberOfDays: !!data.isChecked && [].includes(data.codeDescription),
        showReason: !!data.isChecked && ["Markdown Allowance", "Mkdn Alw Cost Only"].includes(data.codeDescription),
        showVendorEmail: !!data.isChecked && ["Advertising Allowance", "Mark out of Stock", "Markdown Allowance", "Mkdn Alw Cost Only"].includes(data.codeDescription),
        showMarkup: !!data.isChecked && ["Markdown Allowance", "Mkdn Alw Profit Asst - Cst/Rtl"].includes(data.codeDescription),
        showRetail: !!data.isChecked && ["Markdown Allowance", "Mkdn Alw Profit Asst - Cst/Rtl"].includes(data.codeDescription),
        showAvailableCost: !!data.isChecked && ["Ad Coop from Vendor Funding", "Mkdn Alw Profit Asst - Cost", "Mkdn Alw Profit Asst - Cst/Rtl"].includes(data.codeDescription)
    };

    let reasonAvlOptions = [];
    if (!!data.isChecked && ["Ad Coop from Vendor Funding", "Advertising Allowance"].includes(data.codeDescription)) {
        reasonAvlOptions = [{codeValue: "123", codeDescription: "Promotional Allowance"}];
    } else if (!!data.isChecked && ["Mark out of Stock"].includes(data.codeDescription)) {
        reasonAvlOptions = [{codeValue: "20", codeDescription: "Damage Allowance"}];
    } else if (!!data.isChecked && ["Markdown Allowance"].includes(data.codeDescription)) {
        reasonAvlOptions = [{codeValue: "20", codeDescription: "Damage Allowance"}, {codeValue: "10", codeDescription: "Early/Late Ship Allowance"}, {codeValue: "05", codeDescription: "Margin Support"}];
    } else if (!!data.isChecked && ["Mkdn Alw Cost Only"].includes(data.codeDescription)) {
        reasonAvlOptions = [{codeValue: "10", codeDescription: "Early/Late Ship Allowance"}, {codeValue: "05", codeDescription: "Margin Support"}, {codeValue: "40", codeDescription: "New Item Allowance"}, {codeValue: "35", codeDescription: "New Store Allowance"}, {codeValue: "00", codeDescription: "No Reason Specified"}, {codeValue: "25", codeDescription: "Off Price Allowance"}, {codeValue: "15", codeDescription: "Volume Rebate"}];
    } else if (!!data.isChecked && ["Mkdn Alw Profit Asst - Cost", "Mkdn Alw Profit Asst - Cst/Rtl"].includes(data.codeDescription)) {
        reasonAvlOptions = [{codeValue: "05", codeDescription: "Margin Support"}];
    }

    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_CHANGE_DEBIT_TYPE", data, newScreenFields, reasonAvlOptions });
    }
}

export const changeVendorSelected = (data) => {
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_CHANGE_VENDOR", data });
        if (!!data.isChecked) {
            dispatch(getVendorDepartment());
        }
    }
}

export const changeStatus = (data) => {
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_CHANGE_STATUS", data });
    }
}

export const changeCollectionMethod = (data) => {
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_CHANGE_COLLECTION_METHOD", data });
    }
}

export const changeNumberOfDays = (val) => {
    let value = val.replace(/[^0-9]/g,'');
    value = Number(value) > 30 ? "30" : value;
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_CHANGE_NUMBER_OF_DAYS", data: value });
    }
}

export const changeReasons = (data) => {
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_CHANGE_REASONS", data });
    }
} 

export const changeNetCost = (val) => {
    // let value = val.replace(/[^0-9]/g,'');
    // value = Number(value) > 99999999999.99 ? "99999999999.99" : value;
    const value = Number(val);
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_CHANGE_NET_COST", data: value });
        dispatch(changeRetail());
    }
}

export const changeMarkup = (val) => {
    // let value = val.replace(/[^0-9]/g,'');
    // value = Number(value) > 99.99 ? "99.99" : value;
    const value = Number(val);
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_CHANGE_MARKUP", data: value });
        dispatch(changeRetail());
    }
}

export const changeRetail = () => {
    return (dispatch, getState) => {
        const {markup, netCost} = getState().newState;
        const value = markup && netCost ? (1/(1 - (markup/100)) * netCost).toFixed(2) : "";
        dispatch({ type: "DA_NEW_CHANGE_RETAIL", data: value });
    }
}

export const changeAvailableCost = (val) => {
    let value = val.replace(/[^0-9]/g,'');
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_CHANGE_AVAILABLE_COST", data: value });
    }
}

export const changePONumber = (val) => {
    // let value = val.replace(/[^0-9]/g,'');
    const value = Number(val);
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_CHANGE_PO_NUMBER", data: value });
    }
}

export const changeDepartmentSelected = (data) => {
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_CHANGE_DEPARTMENT", data });
        if (!!data.isChecked) {
            dispatch(getVendorEmailAddress());
            dispatch(getSubClass());
            dispatch(getAvailableCost());
            dispatch(getDefaultMarkupPercentage());
        }
    }
}

export const changeSubclassSelected = (data) => {
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_CHANGE_SUBCLASS", data });
    }
}

export const changeNoteType = (data) => {
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_CHANGE_NOTE_TYPE", data });
    }
}

export const changeNoteText = (data) => {
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_CHANGE_NOTE_TEXT", data });
    }
}

export const refreshNotes = () => {
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_REFRESH_NOTES" });
    }
}

export const toggleDACreationStatus = () => {
    return (dispatch, getState) => {
        dispatch({ type: "DA_NEW_TOGGLE_CREATION_STATUS" });
    }
}

export const changeBrandDetails = (val, col, row) => {
    console.log("val, col, row", val, col, row);
    return (dispatch, getState) => {
        let value = Number(val) > 100 ? 100 : Number(val);
        const {brandDetails} = getState().newState;
        const netCost = 100;
        const netCostInd = netCost * value / 100;
        const data = brandDetails.map(brand => brand.rowIndex === row ? {
            "rowIndex": row, 
            "value": [brand.value[0], brand.value[1], value, netCostInd]
        } : brand);
        
        dispatch({ type: "DA_NEW_CHANGE_BRAND_DETAILS", data });
    }
}