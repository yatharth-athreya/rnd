import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Alert, Button, ButtonGroup, Input} from 'reactstrap';
import './SearchDoc.scss';
import RGL, { WidthProvider } from "react-grid-layout";
import CheckboxFilter from '../../../../common/CheckBoxFilter/CheckBoxFilter';
import { PulseLoader } from 'halogenium';
const ReactGridLayout = WidthProvider(RGL);
import "react-select/dist/react-select.css";
import "react-virtualized-select/styles.css";
import Select from "react-virtualized-select";
import {selectType} from "../../../../../actions/search-action";
import SearchGrid from "./SearchGrid";
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import moment from 'moment';
import { DateRangePicker, isInclusivelyAfterDay } from 'react-dates';

class SearchDoc extends Component {
    constructor(props) {
        super(props);
        this.gridLayoutRef = React.createRef();
        this.searchTypeView = this._searchTypeView.bind(this);
        this.state = {
            layout: [
                {"i": "search-doc-aaa", "x": 0, "y": 0, "w": 12, "h": 2, "minW": 12, "minH": 2},
                {"i": "search-doc-bbb", "x": 0, "y": 2, "w": 12, "h": 2, "minW": 12, "minH": 2},
                {"i": "search-doc-ccc", "x": 0, "y": 4, "w": 12, "h": 0.5, "minW": 12, "minH": 0.5, "maxH": 0.5}
            ]
        };
    }

    _searchTypeView(){
        const {selectedType} = this.props;
        return (
            <div className="DA-Search-Type" style={{display: 'flex', justifyContent: 'space-between'}}>
                {selectedType.label === "Search By Buyer" && <CheckboxFilter
                    options={[]}
                    onChange={() => {}}
                    placeholder="Select Buyer" 
                    value={[]}
                    valueKey="value"
                    labelKey="value"
                    classname="DA-ST-Filters"
                    label="Buyer"
                    disabled={false}
                    hideOnSelect={true}
                    multi={false} />}
                {selectedType.label === "Search By DMM" && <CheckboxFilter
                    options={[]}
                    onChange={() => {}}
                    placeholder="Select DMM" 
                    value={[]}
                    valueKey="value"
                    labelKey="value"
                    classname="DA-ST-Filters"
                    label="DMM"
                    disabled={false}
                    hideOnSelect={true}
                    multi={false} />}
                {selectedType.label === "Search By Debit Allowance" && <CheckboxFilter
                    options={[]}
                    onChange={() => {}}
                    placeholder="Select Debit Allowance" 
                    value={[]}
                    valueKey="value"
                    labelKey="value"
                    classname="DA-ST-Filters"
                    label="Debit Allowance"
                    disabled={false}
                    hideOnSelect={true}
                    multi={false} />}
                {selectedType.label === "Search By Department" && <CheckboxFilter
                    options={[]}
                    onChange={() => {}}
                    placeholder="Select Department" 
                    value={[]}
                    valueKey="value"
                    labelKey="value"
                    classname="DA-ST-Filters"
                    label="Department"
                    disabled={false}
                    hideOnSelect={true}
                    multi={false} />}
                {selectedType.label === "Search By Status" && <CheckboxFilter
                    options={[]}
                    onChange={() => {}}
                    placeholder="Select Status" 
                    value={[]}
                    valueKey="value"
                    labelKey="value"
                    classname="DA-ST-Filters"
                    label="Status"
                    disabled={false}
                    hideOnSelect={true}
                    multi={false} />}
                {!["Search By Debit Allowance", "Search By Price Change Doc"].includes(selectedType.label) && <CheckboxFilter
                    options={[]}
                    onChange={() => {}}
                    placeholder="Select Debit Type" 
                    value={[]}
                    valueKey="displayDesc"
                    labelKey="displayDesc"
                    classname="DA-ST-Filters"
                    label="Debit Type"
                    disabled={false}
                    hideOnSelect={true}
                    multi={false} />}
                {!["Search By Debit Allowance", "Search By Price Change Doc"].includes(selectedType.label) && <div className={"DA-ST-Filters"}>
                    <span style={{ fontSize: '12px', fontWeight: 700 }} >{"Effective Start/End Date Range"}</span>
                    <DateRangePicker
                        startDate={null} // momentPropTypes.momentObj or null,
                        startDateId="dla-effective-start-date" // PropTypes.string.isRequired,
                        endDate={null} // momentPropTypes.momentObj or null,
                        endDateId="dla-effective-end-date" // PropTypes.string.isRequired,
                        onDatesChange={({ startDate, endDate }) => {}} // PropTypes.func.isRequired,
                        focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                        onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                        disabled={false}
                        isOutsideRange={day => !isInclusivelyAfterDay(day, moment())}
                        readOnly
                    />
                </div>}
                {selectedType.label === "Search By Department" && <CheckboxFilter
                    options={[]}
                    onChange={() => {}}
                    placeholder="Select Vendor Name" 
                    value={[]}
                    valueKey="value"
                    labelKey="value"
                    classname="DA-ST-Filters"
                    label="Vendor Name"
                    disabled={false}
                    hideOnSelect={true}
                    multi={false} />}
                {selectedType.label === "Search By Price Change Doc" && <CheckboxFilter
                    options={[]}
                    onChange={() => {}}
                    placeholder="Select Price Change Doc" 
                    value={[]}
                    valueKey="value"
                    labelKey="value"
                    classname="DA-ST-Filters"
                    label="Price Change Doc"
                    disabled={false}
                    hideOnSelect={true}
                    multi={false} />}
                <div className="DA-ST-Filters">
                    <Button
                        style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white', marginTop:'25px'}} 
                        size="lg" 
                        color='primary'
                        disabled={false}
                        onClick={() => {}}>
                        {'Search'}
                    </Button>
                </div>
                {["Search By Debit Allowance", "Search By Price Change Doc"].includes(selectedType.label) && <div style={{width:'50%'}}></div>}
                {["Search By Debit Type"].includes(selectedType.label) && <div style={{width:'25%'}}></div>}
            </div>
        ); 
    }
        
    render() {
        const {searchTypeOptions, selectedType} = this.props;
        const searchTypeView = this.searchTypeView();
        return (
            <div className="search-doc">
                <ReactGridLayout ref={this.gridLayoutRef} className="layout" layout={this.state.layout} cols={12} draggableHandle=".MyDragHandleClassName">
                        <div key="search-doc-aaa">
                            <div className="search-doc-child">
                                <div className="search-doc-child-header MyDragHandleClassName">Debit Allowance Search</div>
                                <div id="search-type-select"><Select labelKey='label' valueKey='value' options={searchTypeOptions} onChange={(selectedType) => this.props.selectType({ selectedType })} value={selectedType} clearable={false} /></div>
                                <hr />
                                <div className="search-doc-child-body">
                                    {searchTypeView}
                                </div>
                            </div>
                        </div>
                        <div key="search-doc-bbb">
                            <div className="search-doc-child">
                                <div className="search-doc-child-header MyDragHandleClassName">Result</div>
                                <hr />
                                <div className="search-doc-child-body">
                                    <SearchGrid />
                                </div>
                                <div className="search-doc-child-buttons">
                                    <div className="sdcbtn-Input">
                                        <span style={{ fontSize: '12px', fontWeight: 700 }}>Total DAs</span>
                                        <Input
                                            style={{ height: '34px', borderRadius: '5px' }}
                                            value={this.state.value}
                                            onChange={(e) => {this.changeValue(e)}}
                                            className="idHdr"
                                            bsSize="lg"
                                            name="td"
                                            id="td"
                                            disabled={false}
                                            type="text"
                                            maxLength={7}
                                            placeholder="" />
                                    </div>
                                    <div className="sdcbtn-Input">
                                        <span style={{ fontSize: '12px', fontWeight: 700 }}>Total Net Cost</span>
                                        <Input
                                            style={{ height: '34px', borderRadius: '5px' }}
                                            value={this.state.value}
                                            onChange={(e) => {this.changeValue(e)}}
                                            className="idHdr"
                                            bsSize="lg"
                                            name="tnc"
                                            id="tnc"
                                            disabled={false}
                                            type="text"
                                            maxLength={7}
                                            placeholder="" />
                                    </div>
                                    <div className="sdcbtn-Input">
                                        <span style={{ fontSize: '12px', fontWeight: 700 }}>Total Retail</span>
                                        <Input
                                            style={{ height: '34px', borderRadius: '5px' }}
                                            value={this.state.value}
                                            onChange={(e) => {this.changeValue(e)}}
                                            className="idHdr"
                                            bsSize="lg"
                                            name="tr"
                                            id="tr"
                                            disabled={false}
                                            type="text"
                                            maxLength={7}
                                            placeholder="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div key="search-doc-ccc">
                            <div className="search-doc-child">
                                <div className="search-doc-buttons MyDragHandleClassName">
                                    <ButtonGroup>
                                        <Button
                                            style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                            size="lg" 
                                            color='secondary'
                                            disabled={false}
                                            onClick={() => {}}>
                                            {'Refresh'}
                                        </Button>
                                        <Button
                                            style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                            size="lg" 
                                            color='secondary'
                                            disabled={false}
                                            onClick={() => {}}>
                                            {'Maintenance'}
                                        </Button>
                                        <Button
                                            style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                            size="lg" 
                                            color='secondary'
                                            disabled={false}
                                            onClick={() => {}}>
                                            {'Details'}
                                        </Button>
                                        <Button
                                            style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                            size="lg" 
                                            color='secondary'
                                            disabled={false}
                                            onClick={() => {}}>
                                            {'Cancel'}
                                        </Button>
                                    </ButtonGroup>
                                </div>
                            </div>
                        </div>
                    </ReactGridLayout>
            </div>
        );
    }
}

SearchDoc.propTypes = {}

const mapStateToProps = state => {
    return {
        dlaState: state.dlaState,
        selectedType: state.searchState.selectedType,
        searchTypeOptions: state.searchState.searchTypeOptions
    }
}

const mapDispatchToProps = dispatch => ({
    selectType: (v) => dispatch(selectType(v))
})

export default connect(mapStateToProps, mapDispatchToProps)(SearchDoc);