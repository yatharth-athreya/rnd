import React, { Component } from 'react';
import { AutoSizer, MultiGrid } from 'react-virtualized';
import { connect } from 'react-redux';
import {Input} from 'reactstrap';
import {saveBrands, changeBrandDetails} from '../../../../../actions/new-action';
import _ from 'lodash';

class BrandSearchGrid extends Component {
	constructor(props) {
    	super(props);
        this.state = {}
        this._gridHeight = this._gridHeight.bind(this);
        this._cellRenderer = this._cellRenderer.bind(this);
    }

    componentWillUpdate (nextProps) {
        if (!_.isEqual(nextProps.newState.brandDetails, this.props.newState.brandDetails) && this._grid) {
            this._grid.forceUpdateGrids(); 
        }
    }
    
    _gridHeight (length) {
        const height = length * 30;
        const innerHeight = window.innerHeight - 300 <= 0 ? 300 : window.innerHeight - 300;
        return Math.min(height, innerHeight);
    }

	_cellRenderer({ columnIndex, key, rowIndex, style }) {
        const {brandDetails} = this.props.newState;
        const row = brandDetails.find(e => e.rowIndex === rowIndex);
        const rowClassName = rowIndex === 0 ? "cell-header" : "cell-body";
        return (
            <div key={key} style={style} className={`${rowClassName}`}>
                {columnIndex === 2 && rowIndex !== 0  ? <Input
                    style={{ height: '25px', borderRadius: '5px', width: "250px", fontSize: "12px" }}
                    value={row && row.value && row.value[columnIndex]}
                    onChange={(e) => {this.props.dispatch(changeBrandDetails(e.target.value, columnIndex, rowIndex))}}
                    className="proration"
                    bsSize="lg"
                    name="proration"
                    id={key}
                    disabled={false}
                    type="number"
                    min={0}
                    max={100}
                    placeholder="" /> : 
                row && row.value && row.value[columnIndex]}
            </div>
        );
    }
 
  	render() {        
        const {brandDetails} = this.props.newState;
        
        return (
            <div>
                {!!brandDetails.length ?
                    <div id="BrandSearchGrid">
                        <AutoSizer disableHeight>
                            {({width}) => (
                                <MultiGrid
                                    ref={(ref) => this._grid = ref}
                                    cellRenderer={this._cellRenderer}
                                    columnWidth={226.25}
                                    columnCount={4}
                                    height={this._gridHeight(brandDetails.length) + 15}
                                    rowHeight={30}
                                    rowCount={brandDetails.length}
                                    width={width}
                                    fixedColumnCount={0}
                                    fixedRowCount={1}
                                    hideTopRightGridScrollbar
                                    hideBottomLeftGridScrollbar
                                /> 
                            )}
                        </AutoSizer>                        
                    </div>
                :null}
            </div>
		);
  	}
}

BrandSearchGrid.propTypes = {}

const mapStateToProps = state => {
    return {
        newState: state.newState
    }
}

export default connect(mapStateToProps)(BrandSearchGrid);