import React, { Component } from 'react';
import ReactToPrint from 'react-to-print';
import {Button, Table} from 'reactstrap';
import moment from 'moment';
import { connect } from 'react-redux';

class ComponentToPrint extends Component {
	render() {
		const date = moment().format("MMMM Do YYYY, h:mm:ss a");
		return (
			<div style={{margin: "20px"}}>
				<div style={{marginBottom: "20px"}}>{date}</div>
				<h1 style={{marginBottom: "20px", textAlign:"Center", fontWeight: "700"}}>Debit Allowance Document</h1>
				<h2 style={{fontWeight: "700"}}>General Information</h2>
				<Table>
					<tbody>
						<tr>
							<td scope="row">Vendor Name</td>
							<td>--</td>
						</tr>
						<tr>
							<td scope="row">Debit Allowance</td>
							<td>--</td>
						</tr>
						<tr>
							<td scope="row">Price Chng Document</td>
							<td>--</td>
						</tr>
						<tr>
							<td scope="row">Debit Type</td>
							<td>--</td>
						</tr>
						<tr>
							<td scope="row">Status</td>
							<td>--</td>
						</tr>
						<tr>
							<td scope="row">Created Date</td>
							<td>--</td>
						</tr>
						<tr>
							<td scope="row">Created By</td>
							<td>--</td>
						</tr>
						<tr>
							<td scope="row">DMM Approval Date</td>
							<td>--</td>
						</tr>
						<tr>
							<td scope="row">DMM Approval</td>
							<td>--</td>
						</tr>
						<tr>
							<td scope="row">AP Approval Date</td>
							<td>--</td>
						</tr>
						<tr>
							<td scope="row">AP Approval</td>
							<td>--</td>
						</tr>
						<tr>
							<td scope="row">Effective Date</td>
							<td>--</td>
						</tr>
						<tr>
							<td scope="row">Fiscal Month</td>
							<td>--</td>
						</tr>
						<tr>
							<td scope="row">Collection Method</td>
							<td>--</td>
						</tr>
						<tr>
							<td scope="row">Number of Days</td>
							<td>--</td>
						</tr>
						<tr>
							<td scope="row">Reason</td>
							<td>--</td>
						</tr>
					</tbody>
				</Table>

				<h2 style={{fontWeight: "700"}}>Debit Allowance Value</h2>
				<Table>
					<tbody>
						<tr>
							<td scope="row">Net Cost</td>
							<td>--</td>
						</tr>
						<tr>
							<td scope="row">Markup Percent</td>
							<td>--</td>
						</tr>
						<tr>
							<td scope="row">Retail</td>
							<td>--</td>
						</tr>
					</tbody>
				</Table>
			</div>
		);
	}
}
 
class Print extends Component {
	render() {
		return (
			<div>
				<ReactToPrint
				trigger={() => <Button color="primary" style={{fontSize:'12px'}}>Print</Button>}
				content={() => this.componentRef}
				/>
				<div style={{ display: "none" }}><ComponentToPrint ref={el => (this.componentRef = el)} {...this.props} /></div>
			</div>
		);
	}
}

Print.propTypes = {}

const mapStateToProps = state => {
    return {
		effectiveDate: state.newState.effectiveDate,
		showPrintReportsModal: state.newState.showPrintReportsModal
    }
}

export default connect(mapStateToProps)(Print);