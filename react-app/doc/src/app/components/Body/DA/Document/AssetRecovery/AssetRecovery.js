import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Alert, Button, ButtonGroup, Input, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import './AssetRecovery.scss';
import RGL, { WidthProvider } from "react-grid-layout";
import { PulseLoader } from 'halogenium';
const ReactGridLayout = WidthProvider(RGL);
import "react-select/dist/react-select.css";
import "react-virtualized-select/styles.css";
import Select from "react-virtualized-select";
import {selectType} from "../../../../../actions/search-action";
import ARGrid from "./ARGrid";

class AssetRecovery extends Component {
    constructor(props) {
        super(props);
	    this.toggleNotes = this.toggleNotes.bind(this);
        this.changeValue = this.changeValue.bind(this);
        this.gridLayoutRef = React.createRef();
        this.state = {
	    	modal: false,
            layout: [
                {"i": "asset-recovery-aaa", "x": 0, "y": 0, "w": 12, "h": 2, "minW": 12, "minH": 2},
                {"i": "asset-recovery-bbb", "x": 0, "y": 2, "w": 12, "h": 2, "minW": 12, "minH": 2},
                {"i": "asset-recovery-ccc", "x": 0, "y": 4, "w": 12, "h": 0.5, "minW": 12, "minH": 0.5, "maxH": 0.5}
            ],
            value: ''
        };
    }

	toggleNotes() {
	    this.setState({ modal: !this.state.modal });
	}

    changeValue(e) {
        this.setState({ value: e.target.value });
    }
        
    render() {
        const {searchTypeOptions, selectedType} = this.props;
        return (
            <div className="asset-recovery">
                <ReactGridLayout ref={this.gridLayoutRef} className="layout" layout={this.state.layout} cols={12} draggableHandle=".MyDragHandleClassName">
                    <div key="asset-recovery-aaa">
                        <div className="asset-recovery-child">
                            <div className="asset-recovery-child-header MyDragHandleClassName">Debit Allowance Asset Recovery</div>
                            <hr />
                            <div className="asset-recovery-child-body">
                                <div className="arcb-Input">
                                    <span style={{ fontSize: '12px', fontWeight: 700 }}>Debit Type</span>
                                    <Input
                                        style={{ height: '34px', borderRadius: '5px' }}
                                        value={this.state.value}
                                        onChange={(e) => {this.changeValue(e)}}
                                        className="idHdr"
                                        bsSize="lg"
                                        name="DT"
                                        id="DT"
                                        disabled={false}
                                        type="text"
                                        maxLength={7}
                                        placeholder="" />
                                </div>
                                <div className="arcb-Input">
                                    <span style={{ fontSize: '12px', fontWeight: 700 }}>Status</span>
                                    <Input
                                        style={{ height: '34px', borderRadius: '5px' }}
                                        value={this.state.value}
                                        onChange={(e) => {this.changeValue(e)}}
                                        className="idHdr"
                                        bsSize="lg"
                                        name="Sts"
                                        id="Sts"
                                        disabled={false}
                                        type="text"
                                        maxLength={7}
                                        placeholder="" />
                                </div>
                                <div className="arcb-Input">
                                    <span style={{ fontSize: '12px', fontWeight: 700 }}>Vendor</span>
                                    <Input
                                        style={{ height: '34px', borderRadius: '5px' }}
                                        value={this.state.value}
                                        onChange={(e) => {this.changeValue(e)}}
                                        className="idHdr"
                                        bsSize="lg"
                                        name="Vndr"
                                        id="Vndr"
                                        disabled={false}
                                        type="text"
                                        maxLength={7}
                                        placeholder="" />
                                </div>
                                <div className="arcb-Input">
                                    <span style={{ fontSize: '12px', fontWeight: 700 }}>Reason</span>
                                    <Input
                                        style={{ height: '34px', borderRadius: '5px' }}
                                        value={this.state.value}
                                        onChange={(e) => {this.changeValue(e)}}
                                        className="idHdr"
                                        bsSize="lg"
                                        name="rsn"
                                        id="rsn"
                                        disabled={false}
                                        type="text"
                                        maxLength={7}
                                        placeholder="" />
                                </div>
                                <div className="arcb-Input">
                                    <span style={{ fontSize: '12px', fontWeight: 700 }}>Effective Date</span>
                                    <Input
                                        style={{ height: '34px', borderRadius: '5px' }}
                                        value={this.state.value}
                                        onChange={(e) => {this.changeValue(e)}}
                                        className="idHdr"
                                        bsSize="lg"
                                        name="ed"
                                        id="ed"
                                        disabled={false}
                                        type="text"
                                        maxLength={7}
                                        placeholder="" />
                                </div>
                                <div className="arcb-Input">
                                    <span style={{ fontSize: '12px', fontWeight: 700 }}>Collection Method</span>
                                    <Input
                                        style={{ height: '34px', borderRadius: '5px' }}
                                        value={this.state.value}
                                        onChange={(e) => {this.changeValue(e)}}
                                        className="idHdr"
                                        bsSize="lg"
                                        name="cm"
                                        id="cm"
                                        disabled={false}
                                        type="text"
                                        maxLength={7}
                                        placeholder="" />
                                </div>
                                <div className="arcb-Input">
                                    <span style={{ fontSize: '12px', fontWeight: 700 }}>Number of Days</span>
                                    <Input
                                        value={this.state.value}
                                        onChange={(e) => {this.changeValue(e)}}
                                        className="idHdr"
                                        bsSize="lg"
                                        name="NoD"
                                        id="NoD"
                                        disabled={false}
                                        type="text"
                                        maxLength={7}
                                        placeholder="" />
                                </div>
                                <div className="arcb-Input">
                                    <span style={{ fontSize: '12px', fontWeight: 700 }}>Fiscal Month</span>
                                    <Input
                                        style={{ height: '34px', borderRadius: '5px' }}
                                        value={this.state.value}
                                        onChange={(e) => {this.changeValue(e)}}
                                        className="idHdr"
                                        bsSize="lg"
                                        name="fm"
                                        id="fm"
                                        disabled={false}
                                        type="text"
                                        maxLength={7}
                                        placeholder="" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div key="asset-recovery-bbb">
                        <div className="asset-recovery-child">
                            <div className="asset-recovery-child-header MyDragHandleClassName">Debit Allowance Value</div>
                            <hr />
                            <ARGrid />
                        </div>
                    </div>
                    <div key="asset-recovery-ccc">
                        <div className="asset-recovery-child">
                            <div className="asset-recovery-buttons MyDragHandleClassName">
                                <ButtonGroup>
                                    <Button
                                        style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                        size="lg" 
                                        color='primary'
                                        disabled={false}
                                        onClick={() => {}}>
                                        {'OK'}
                                    </Button>
                                    <Button
                                        style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                        size="lg" 
                                        color='secondary'
                                        disabled={false}
                                        onClick={() => {}}>
                                        {'Add'}
                                    </Button>
                                    <Button
                                        style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                        size="lg" 
                                        color='secondary'
                                        disabled={false}
                                        onClick={() => {}}>
                                        {'Delete'}
                                    </Button>
                                    <Button
                                        style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                        size="lg" 
                                        color='secondary'
                                        disabled={false}
                                        onClick={() => {}}>
                                        {'Refresh'}
                                    </Button>
                                    <Button
                                        style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                        size="lg" 
                                        color='secondary'
                                        disabled={false}
                                        onClick={() => {this.toggleNotes()}}>
                                        {'Notes'}
                                    </Button>
                                    <Button
                                        style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                        size="lg" 
                                        color='secondary'
                                        disabled={false}
                                        onClick={() => {}}>
                                        {'Cancel'}
                                    </Button>
                                </ButtonGroup>
                            </div>
                        </div>
                    </div>
                </ReactGridLayout>
                <Modal isOpen={this.state.modal} toggle={() => {this.toggleNotes()}} className={this.props.className}>
		        	<ModalHeader toggle={() => {this.toggleNotes()}}>Debit Allowance Notes Entry</ModalHeader>
		        	<ModalBody>
                        <Input type="textarea" name="text" id="exampleText" />
		          	</ModalBody>
		          	<ModalFooter>
		            	<Button color="primary" onClick={() => {this.toggleNotes()}}>OK</Button>
                        <Button color="secondary" onClick={() => {this.toggleNotes()}}>Refresh</Button>
                        <Button color="secondary" onClick={() => {this.toggleNotes()}}>Cancel</Button>
		          	</ModalFooter>
		        </Modal>
            </div>
        );
    }
}

AssetRecovery.propTypes = {}

const mapStateToProps = state => {
    return {
        dlaState: state.dlaState,
    }
}

const mapDispatchToProps = dispatch => ({
    selectType: (v) => dispatch(selectType(v))
})

export default connect(mapStateToProps, mapDispatchToProps)(AssetRecovery);