import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Alert, Button, ButtonGroup, Col, Form, FormGroup, Input, Label, Modal, ModalHeader, ModalBody, ModalFooter, Row} from 'reactstrap';
import './New.scss';
import RGL, { WidthProvider } from "react-grid-layout";
import { PulseLoader } from 'halogenium';
import CheckboxFilter from '../../../../common/CheckBoxFilter/CheckBoxFilter';
const ReactGridLayout = WidthProvider(RGL);
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import { DateRangePicker, SingleDatePicker, DayPickerRangeController, isInclusivelyBeforeDay } from 'react-dates';
import moment, { relativeTimeThreshold } from 'moment';
import {toggleNotes, toggleBrand, onChangeEffectiveDate, fetchCreateDAFormData, saveDebitAllowance, togglePrintConfirmationModal, togglePrintReportsModal, validateDACreation, changeDebitTypeSelected, changeVendorSelected, changeCollectionMethod, changeReasons, refreshNew, changeNumberOfDays, changeStatus, changeNetCost, changeMarkup, changeRetail, changePONumber, changeAvailableCost, toggleExitConfirmationModal, takeMeHome, changeDepartmentSelected, changeSubclassSelected, changeNoteType, saveNotes, refreshNotes, changeNoteText, saveBrands, toggleDACreationStatus} from '../../../../../actions/new-action';
import BrandSearchGrid from "./BrandSearchGrid";
import Print from "./Print";
import axios from "axios";

class New extends Component {
    constructor(props) {
        super(props);
        this.populateHeaders = this.populateHeaders.bind(this);
        this.onFocusChange = this.onFocusChange.bind(this);
        this.changeValue = this.changeValue.bind(this);
        this.gridLayoutRef = React.createRef();
        this.state = {
            layout: [
                {"i": "NewAAA", "x": 0, "y": 0, "w": 12, "h": 2, "minW": 12, "minH": 2},
                {"i": "NewBBB", "x": 0, "y": 2, "w": 12, "h": 1, "minW": 12, "minH": 1},
                {"i": "NewCCC", "x": 0, "y": 3, "w": 12, "h": 1.25, "minW": 12, "minH": 1.25},
                {"i": "NewDDD", "x": 0, "y": 4.25, "w": 12, "h": 0.5, "minW": 12, "minH": 0.5, "maxH": 1}
            ],
            brandLayout: [
                {"i": "BrandAAA", "x": 0, "y": 0, "w": 12, "h": 1, "minW": 12, "minH": 1},
                {"i": "BrandBBB", "x": 0, "y": 2, "w": 12, "h": 2, "minW": 12, "minH": 2}
            ],
            value: '',
            focused: false
        };
    }

    populateHeaders(getHeadersVal){
        axios.defaults.headers.common['Authorization'] = !!getHeadersVal['Authorization'] ? getHeadersVal['Authorization'] : '';
        axios.defaults.headers.common['Delivery-Date'] = !!getHeadersVal['Delivery-Date'] ? getHeadersVal['Delivery-Date'] : '';
        axios.defaults.headers.common['From'] = !!getHeadersVal['From'] ? getHeadersVal['From'] : '';
        axios.defaults.headers.common['X-Correlation-ID'] = !!getHeadersVal['X-Correlation-ID'] ? getHeadersVal['X-Correlation-ID'] : '';
        this.props.dispatch(fetchCreateDAFormData());
    }

    componentWillMount() {        
        var finalHeaders = {};
        var self = this;
        var request = window.indexedDB.open("DasDataBase", 10);
        var db;

        request.onsuccess = function () {
            db = request.result;

            var transaction = db.transaction(['daui']);
            var objectStore = transaction.objectStore('daui');
            var request1 = objectStore.get(1);
         
            request1.onerror = function() {
                return {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',        
                    "Authorization": '',
                    "Delivery-Date": '',
                    "From": '',
                    "X-Correlation-ID": ''
                };
            };
     
            request1.onsuccess = function() {
                if (request1.result) {
                    finalHeaders = request1.result.headers;
                    self.populateHeaders(finalHeaders);
                    return request1.result.headers;
                } else {
                    return {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',        
                        "Authorization": '',
                        "Delivery-Date": '',
                        "From": '',
                        "X-Correlation-ID": ''
                    };
                }
            };
        }

        request.onerror = function () {
            return {
                'Accept': 'application/json',
                'Content-Type': 'application/json',        
                "Authorization": '',
                "Delivery-Date": '',
                "From": '',
                "X-Correlation-ID": ''
            }; 
        }
    }

    onFocusChange(obj){
        let focused = obj.focused;
        this.setState({ focused });
        // this.props.onFocusChange(focused);
    }

    changeValue(e) {
        this.setState({ value: e.target.value });
    }
        
    render() {
        const {debitAllowanceValue, priceChangeDocumentValue, effectiveDate, debitTypeOptions, debitTypeSelected, vendorOptions, vendorSelected, collectionMethodOptions, collectionMethodSelected, reasonAvlOptions, reasonSelected, numberOfDays, statusOptions, statusSelected, newScreenFields, netCost, markup, retail, poNumber, avlCost, fiscalMonth, vendorEmail, showNotesModal, showBrandModal, showPrintConfirmationModal, showPrintReportsModal, daCreateValidationError, showExitConfirmationModal, departmentOptions, departmentSelected, subclassOptions, subclassSelected, noteTypeOptions, noteTypeSelected, noteText, newDAStatus, brandDetailsProrationFlag, notesErrorFlag} = this.props.newState;
        return (
            <div className="new-document" style={{marginTop:"-10px"}}>
                <ReactGridLayout ref={this.gridLayoutRef} className="layout" layout={this.state.layout} cols={12} draggableHandle=".MyDragHandleClassName">
                        <div key="NewAAA">
                            <div className="new-document-child">
                                <div className="MyDragHandleClassName" id="new-document-header">Debit Allowance Entry</div>
                                <hr />
                                <div className="DA-New">
                                        {/*<div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Debit Allowance</span>
                                            <Input
                                                style={{ height: '29px', borderRadius: '5px' }}
                                                value={debitAllowanceValue}
                                                onChange={(e) => {this.changeValue(e)}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="da"
                                                id="da"
                                                disabled={true}
                                                type="text"
                                                maxLength={7}
                                                placeholder="Debit Allowance" />
                                        </div>*/}
                                        {/*<div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Price Change Document</span>
                                            <Input
                                                style={{ height: '29px', borderRadius: '5px' }}
                                                value={priceChangeDocumentValue}
                                                onChange={(e) => {this.changeValue(e)}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="pcd"
                                                id="pcd"
                                                disabled={true}
                                                type="text"
                                                maxLength={7}
                                                placeholder="Price Change Document" />
                                        </div>*/}
                                        <CheckboxFilter
                                            options={debitTypeOptions}
                                            onChange={(value) => {this.props.dispatch(changeDebitTypeSelected(value))}}
                                            placeholder="" 
                                            value={debitTypeSelected}
                                            valueKey="codeDescription"
                                            labelKey="codeDescription"
                                            classname="DA-New-Input"
                                            label="Debit Type:"
                                            disabled={false}
                                            hideOnSelect={true}
                                            multi={false} />
                                        <CheckboxFilter
                                            options={vendorOptions}
                                            onChange={(value) => {this.props.dispatch(changeVendorSelected(value))}}
                                            placeholder="" 
                                            value={vendorSelected}
                                            valueKey="name"
                                            labelKey="name"
                                            classname="DA-New-Input"
                                            label="Vendor:"
                                            disabled={false}
                                            hideOnSelect={true}
                                            multi={false} />
                                        <CheckboxFilter
                                            options={statusOptions}
                                            onChange={(value) => {this.props.dispatch(changeStatus(value))}}
                                            placeholder="" 
                                            value={statusSelected}
                                            valueKey="codeDescription"
                                            labelKey="codeDescription"
                                            classname="DA-New-Input"
                                            label="Status:"
                                            disabled={true}
                                            hideOnSelect={true}
                                            multi={false} />
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700, textAlign: "right", width: "101px", marginRight: "20px", paddingTop: "5px" }}>Effective Date:</span>
                                            <SingleDatePicker
                                                date={effectiveDate}
                                                onDateChange={date => this.props.dispatch(onChangeEffectiveDate(date))}
                                                focused={this.state.focused}
                                                onFocusChange={this.onFocusChange}
                                                readOnly={true}
                                                placeholder=""
                                                numberOfMonths={1}
                                                id="effective_date_id"
                                                isOutsideRange={day => {return false}}
                                            />
                                        </div>
                                        <div className="DA-New-Input" style={{position: "absolute", right: "20px", top: "148px"}}>
                                            <span style={{ fontSize: '12px', fontWeight: 700, textAlign: "right", width: "101px", marginRight: "20px", paddingTop: "5px" }}>Fiscal Month:</span>
                                            <Input
                                                style={{ height: '29px', borderRadius: '5px', width: '250px' }}
                                                value={fiscalMonth ? (fiscalMonth.fiscalMonth + " - " + fiscalMonth.fiscalMonthName ): ""}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="da"
                                                id="fm"
                                                disabled={false}
                                                readOnly={true}
                                                type="text"
                                                placeholder="" />
                                        </div>
                                        <CheckboxFilter
                                            options={collectionMethodOptions}
                                            onChange={(value) => {this.props.dispatch(changeCollectionMethod(value))}}
                                            placeholder="" 
                                            value={collectionMethodSelected}
                                            valueKey="codeDescription"
                                            labelKey="codeDescription"
                                            classname="DA-New-Input"
                                            label="Collection Method:"
                                            disabled={false}
                                            hideOnSelect={true}
                                            multi={false} />
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700, textAlign: "right", width: "101px", marginRight: "20px", paddingTop: "5px" }}>Number of Days:</span>
                                            <Input
                                                style={{ height: '29px', borderRadius: '5px', width: '250px' }}
                                                value={numberOfDays}
                                                onChange={(e) => {this.props.dispatch(changeNumberOfDays(e.target.value))}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="dt"
                                                id="nod"
                                                disabled={!(!!collectionMethodSelected.length && ["02", "03"].includes(collectionMethodSelected[0].codeValue))}
                                                type="text"
                                                min={"1"}
                                                max={"30"}
                                                placeholder="" />
                                        </div>
                                        <CheckboxFilter
                                            options={reasonAvlOptions}
                                            onChange={(value) => {this.props.dispatch(changeReasons(value))}}
                                            placeholder="" 
                                            value={reasonSelected}
                                            valueKey="codeDescription"
                                            labelKey="codeDescription"
                                            classname="DA-New-Input"
                                            label="Reason:"
                                            disabled={[0, 1].includes(reasonAvlOptions.length)}
                                            hideOnSelect={true}
                                            multi={false} />
                                        {/*<div className="DA-New-Input"></div>
                                        <div className="DA-New-Input"></div>*/}
                                </div>
                            </div>
                        </div>
                        <div key="NewBBB">
                            <div className="new-document-child">
                                <div className="MyDragHandleClassName" id="new-document-header">Proration Level</div>
                                <hr />
                                <div className="DA-New">
                                    <CheckboxFilter
                                        options={departmentOptions}
                                        onChange={(value) => {this.props.dispatch(changeDepartmentSelected(value))}}
                                        placeholder="" 
                                        value={departmentSelected}
                                        valueKey="department"
                                        labelKey="department"
                                        classname="DA-New-Input"
                                        label="Department:"
                                        disabled={false}
                                        hideOnSelect={true}
                                        multi={false} />
                                    <CheckboxFilter
                                        options={subclassOptions}
                                        onChange={(value) => {this.props.dispatch(changeSubclassSelected(value))}}
                                        placeholder="" 
                                        value={subclassSelected}
                                        valueKey="subclass"
                                        labelKey="subclass"
                                        classname="DA-New-Input"
                                        label="Subclass:"
                                        disabled={false}
                                        hideOnSelect={true}
                                        multi={false} />
                                    {newScreenFields.showVendorEmail && <div className="DA-New-Input">
                                        <span style={{ fontSize: '12px', fontWeight: 700, textAlign: "right", width: "101px", marginRight: "20px", paddingTop: "5px" }}>Vendor E-mail:</span>
                                        <Input
                                            style={{ height: '29px', borderRadius: '5px', width: '350px' }}
                                            value={vendorEmail}
                                            className="idHdr"
                                            bsSize="lg"
                                            name="dt"
                                            id="vem"
                                            disabled={false}
                                            readOnly={true}
                                            type="text"
                                            placeholder="" />
                                    </div>}
                                </div>
                            </div>
                        </div>
                        <div key="NewCCC">
                            <div className="new-document-child">
                                <div className="MyDragHandleClassName" id="new-document-header">Debit Allowance Value</div>
                                <hr />
                                <div className="DA-New">
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700, textAlign: "right", width: "101px", marginRight: "20px", paddingTop: "5px" }}>Net Cost:</span>
                                            <Input
                                                style={{ height: '29px', borderRadius: '5px', width: "250px", fontSize: "12px" }}
                                                value={netCost}
                                                onChange={(e) => {this.props.dispatch(changeNetCost(e.target.value))}}
                                                className="netcost"
                                                bsSize="lg"
                                                name="netcost"
                                                id="netcost"
                                                disabled={false}
                                                type="number"
                                                min={0}
                                                max={9999999999999}
                                                placeholder="" />
                                        </div>
                                        {newScreenFields.showMarkup && <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700, textAlign: "right", width: "101px", marginRight: "20px", paddingTop: "5px" }}>Markup %:</span>
                                            <Input
                                                style={{ height: '29px', borderRadius: '5px', width: "250px", fontSize: "12px" }}
                                                value={markup}
                                                onChange={(e) => {this.props.dispatch(changeMarkup(e.target.value))}}
                                                className="markup"
                                                bsSize="lg"
                                                name="markup"
                                                id="markup"
                                                disabled={false}
                                                type="number"
                                                min={0}
                                                max={99.99}
                                                placeholder="" />
                                        </div>}
                                        {newScreenFields.showRetail && <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700, textAlign: "right", width: "101px", marginRight: "20px", paddingTop: "5px" }}>Retail:</span>
                                            <Input
                                                style={{ height: '29px', borderRadius: '5px', width: "250px", fontSize: "12px" }}
                                                value={retail}
                                                onChange={(e) => {this.props.dispatch(changeRetail(e.target.value))}}
                                                className="retail"
                                                bsSize="lg"
                                                name="retail"
                                                id="retail"
                                                disabled={true}
                                                type="number"
                                                min={0}
                                                max={999999999999999}
                                                placeholder="" />
                                        </div>}
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700, textAlign: "right", width: "101px", marginRight: "20px", paddingTop: "5px" }}>PO Number:</span>
                                            <Input
                                                style={{ height: '29px', borderRadius: '5px', width: '250px' }}
                                                value={poNumber}
                                                onChange={(e) => {this.props.dispatch(changePONumber(e.target.value))}}
                                                className="ponumber"
                                                bsSize="lg"
                                                name="ponumber"
                                                id="ponumber"
                                                disabled={false}
                                                type="number"
                                                min={0}
                                                max={9999999999}
                                                placeholder="" />
                                        </div>
                                        {newScreenFields.showAvailableCost && <div className="DA-New-Input" style={{position: "absolute", right: "20px", top: "45px"}}>
                                            <span style={{ fontSize: '12px', fontWeight: 700, textAlign: "right", width: "101px", marginRight: "20px", paddingTop: "5px" }}>Available Cost:</span>
                                            <Input
                                                style={{ height: '29px', borderRadius: '5px', width: '250px' }}
                                                value={avlCost}
                                                onChange={(e) => {this.props.dispatch(changeAvailableCost(e.target.value))}}
                                                className="avlcost"
                                                bsSize="lg"
                                                name="avlcost"
                                                id="avlcost"
                                                disabled={false}
                                                readOnly={true}
                                                type="number"
                                                min={0}
                                                max={999999999999999}
                                                placeholder="" />
                                        </div>}
                                </div>
                            </div>
                        </div>
                        <div key="NewDDD">
                            <div className="new-document-child">
                                <div className="DA-New-Buttons MyDragHandleClassName">
                                <ButtonGroup>
                                    <Button
                                        title = {false ? "Please select at least one product" : ""}
                                        style={{}}
                                        size="lg" 
                                        color='primary'
                                        disabled={false}
                                        onClick={() => {this.props.dispatch(saveDebitAllowance())}}>
                                        {'OK'}
                                    </Button>
                                    {/*<Button
                                        style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                        size="lg" 
                                        color='secondary'
                                        disabled={false}
                                        onClick={() => {}}>
                                        {'OK + Repeat'}
                                    </Button>*/}
                                    <Button
                                        style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                        size="lg" 
                                        color='secondary'
                                        disabled={false}
                                        onClick={() => {this.props.dispatch(refreshNew())}}>
                                        {'Refresh'}
                                    </Button>
                                    <Button
                                        style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                        size="lg" 
                                        color='secondary'
                                        disabled={false}
                                        onClick={() => {this.props.dispatch(toggleNotes())}}>
                                        {'Notes'}
                                    </Button>
                                    <Button
                                        style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                        size="lg" 
                                        color='secondary'
                                        disabled={false}
                                        onClick={() => {this.props.dispatch(toggleBrand())}}>
                                        {'Brand'}
                                    </Button>
                                    {/*<Button
                                        style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                        size="lg" 
                                        color='secondary'
                                        disabled={false}
                                        onClick={() => {}}>
                                        {'Delete'}
                                    </Button>*/}
                                    <Button
                                        style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                        size="lg" 
                                        color='secondary'
                                        disabled={false}
                                        onClick={() => {this.props.dispatch(toggleExitConfirmationModal())}}>
                                        {'Cancel'}
                                    </Button>
                                </ButtonGroup>
                                </div>
                            </div>
                        </div>
                </ReactGridLayout>
                <Modal isOpen={showNotesModal} toggle={() => {this.props.dispatch(toggleNotes())}} className={this.props.className}>
		        	<ModalHeader toggle={() => {this.props.dispatch(toggleNotes())}}>Debit Allowance Notes Entry</ModalHeader>
		        	<ModalBody>
                        {notesErrorFlag && <Alert color='warning'>
                            <div style={{display: 'flex'}}>
                                <i className="fa fa-exclamation-triangle" style={{marginRight: '10px', paddingTop:'4px'}}></i>
                                {'You must fill in all of the fields.'}
                            </div>
                        </Alert>}
                        <CheckboxFilter
                            options={noteTypeOptions}
                            onChange={(value) => {this.props.dispatch(changeNoteType(value))}}
                            placeholder="" 
                            value={noteTypeSelected}
                            valueKey="value"
                            labelKey="value"
                            classname="DA-New-Input"
                            label="Note Type"
                            disabled={false}
                            hideOnSelect={true}
                            multi={false} />
                        <hr />
                        {!!noteTypeSelected.length && noteTypeSelected[0].value === "Credit Auth Number" && <h5>Reminder: These notes are printed on the Vendor Remittance Document.</h5>}
                        <Input type="textarea" name="text" id="exampleText" rows="5" maxLength={1000} onChange={(e) => {this.props.dispatch(changeNoteText(e.target.value))}} value={noteText} />
		          	</ModalBody>
		          	<ModalFooter>
		            	<Button color="primary" onClick={() => {this.props.dispatch(saveNotes())}}>OK</Button>
                        <Button color="secondary" onClick={() => {this.props.dispatch(refreshNotes())}}>Refresh</Button>
                        <Button color="secondary" onClick={() => {this.props.dispatch(toggleNotes())}}>Cancel</Button>
		          	</ModalFooter>
		        </Modal>

                <Modal isOpen={showBrandModal} toggle={() => {this.props.dispatch(toggleBrand())}} className={"Brand-Modal"}>
		        	<ModalHeader toggle={() => {this.props.dispatch(toggleBrand())}}>Debit Allowance Brand Entry</ModalHeader>
		        	<ModalBody>
                        <ReactGridLayout ref={this.gridLayoutRef} className="layout" layout={this.state.brandLayout} cols={12} draggableHandle=".MyDragHandleClassName">
                            <div key="BrandAAA">
                                <div className="react-grid-custom-child">
                                    <div className="MyDragHandleClassName" id="DA-new-brand-header">Brand Search Criteria</div>
                                    <hr />
                                    <div className="DA-New-Brand">
                                        <div className="DA-New-Brand-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Department</span>
                                            <Input
                                                style={{ height: '29px', borderRadius: '5px' }}
                                                value={this.state.value}
                                                onChange={(e) => {this.changeValue(e)}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="Department"
                                                id="Department"
                                                disabled={true}
                                                placeholder="" />
                                        </div>
                                        <div className="DA-New-Brand-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Debit Type</span>
                                            <Input
                                                style={{ height: '29px', borderRadius: '5px' }}
                                                value={this.state.value}
                                                onChange={(e) => {this.changeValue(e)}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="DebitType"
                                                id="DebitType"
                                                disabled={true}
                                                placeholder="" />
                                        </div>
                                        <div className="DA-New-Brand-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Vendor Name</span>
                                            <Input
                                                style={{ height: '29px', borderRadius: '5px' }}
                                                value={this.state.value}
                                                onChange={(e) => {this.changeValue(e)}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="VendorName"
                                                id="VendorName"
                                                disabled={true}
                                                placeholder="" />
                                        </div>
                                        <div className="DA-New-Brand-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Debit Allowance</span>
                                            <Input
                                                style={{ height: '29px', borderRadius: '5px' }}
                                                value={this.state.value}
                                                onChange={(e) => {this.changeValue(e)}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="DebitAllowance"
                                                id="DebitAllowance"
                                                disabled={true}
                                                placeholder="" />
                                        </div>
                                        <div className="DA-New-Brand-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Net Cost</span>
                                            <Input
                                                style={{ height: '29px', borderRadius: '5px' }}
                                                value={this.state.value}
                                                onChange={(e) => {this.changeValue(e)}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="NetCost"
                                                id="NetCost"
                                                disabled={true}
                                                placeholder="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div key="BrandBBB">
                                <div className="react-grid-custom-child">
                                    <div className="MyDragHandleClassName" id="DA-new-brand-header">Brand Search Results</div>
                                    <hr />
                                    {brandDetailsProrationFlag && <Alert color='warning'>
                                        <div style={{display: 'flex'}}>
                                            <i className="fa fa-exclamation-triangle" style={{marginRight: '10px', paddingTop:'4px'}}></i>
                                            {'The sum of Proration% should be equal to 100.'}
                                        </div>
                                    </Alert>}
                                    <div className="DA-New-Brand">
                                        <BrandSearchGrid />
                                    </div>
                                </div>
                            </div>
                        </ReactGridLayout>
                    </ModalBody>
		          	<ModalFooter>
		            	<Button color="primary" onClick={() => {this.props.dispatch(saveBrands())}}>Add</Button>
                        <Button color="secondary" onClick={() => {this.props.dispatch(toggleBrand())}}>Cancel</Button>
		          	</ModalFooter>
		        </Modal>

                <Modal isOpen={showPrintConfirmationModal} toggle={() => {this.props.dispatch(togglePrintConfirmationModal())}} className={this.props.className}>
		        	<ModalHeader toggle={() => {this.props.dispatch(togglePrintConfirmationModal())}}>Debit Allowance Yes-No</ModalHeader>
		        	<ModalBody>
                        <div>Debit Allowance -- successfully saved. Do you want to print this Debit Allowance?</div>
                    </ModalBody>
		          	<ModalFooter>
		            	<Button color="primary" onClick={() => {this.props.dispatch(togglePrintReportsModal(true))}}>Yes</Button>
                        <Button color="secondary" onClick={() => {this.props.dispatch(togglePrintConfirmationModal())}}>No</Button>
		          	</ModalFooter>
		        </Modal>

                <Modal isOpen={showExitConfirmationModal} toggle={() => {this.props.dispatch(toggleExitConfirmationModal())}} className={this.props.className}>
		        	<ModalHeader toggle={() => {this.props.dispatch(toggleExitConfirmationModal())}}>Exit Yes-No</ModalHeader>
		        	<ModalBody>
                        <div>Do you want to exit Debit Allowance Entry?</div>
                    </ModalBody>
		          	<ModalFooter>
		            	<Button color="primary" onClick={() => {this.props.dispatch(takeMeHome())}}>Yes</Button>
                        <Button color="secondary" onClick={() => {this.props.dispatch(toggleExitConfirmationModal())}}>No</Button>
		          	</ModalFooter>
		        </Modal>

                <Modal isOpen={daCreateValidationError} toggle={() => {this.props.dispatch(validateDACreation())}} className={this.props.className}>
		        	<ModalHeader toggle={() => {this.props.dispatch(validateDACreation())}}>Errors</ModalHeader>
		        	<ModalBody>
                        { !debitTypeSelected.length && <div>A valid Debit Type must be selected.</div>}
                        { !vendorSelected.length && <div>A valid Vendor must be selected.</div>}
                        { !effectiveDate && <div>Effective Date is required.</div>}
                        { !collectionMethodSelected.length && <div>A valid Collection Method must be selected.</div>}
                        { !netCost && <div>Net Cost is required.</div>}
                        { !departmentSelected.length && <div>Department is required.</div>}
                        { !subclassSelected.length && <div>Subclass is required.</div>}
                        { !(newScreenFields.showVendorEmail && vendorEmail) && <div>Vendor E-mail is required.</div>}
                    </ModalBody>
		          	<ModalFooter>
		            	<Button color="primary" onClick={() => {this.props.dispatch(validateDACreation(true))}}>OK</Button>
		          	</ModalFooter>
		        </Modal>

                <Modal isOpen={["Error", "Loading", "success"].includes(newDAStatus)} onClick={() => {this.props.dispatch(toggleDACreationStatus())}} className={this.props.className}>
		        	<ModalHeader onClick={() => {this.props.dispatch(toggleDACreationStatus())}}>Debit Allowance NEW Entry</ModalHeader>
		        	<ModalBody>
                        <Alert color={newDAStatus === "Loading" ? 'info' : newDAStatus === "Error" ? 'danger' : newDAStatus === "success" ? 'success' : ''}>
                            <div style={{display: 'flex'}}>
                                <i className={newDAStatus === "Loading" ? "" : newDAStatus === "success" ? 'fa fa-check' : "fa fa-exclamation-triangle"} style={{marginRight: (newDAStatus === "Loading" ? "" :'10px'),paddingTop:'4px'}}></i>
                                {newDAStatus === "Loading" ? 'Please wait while we are processing your request' : newDAStatus === "Error" ? 'An error occurred while creating the Debit Allowance.': newDAStatus === "success" ? 'Debit Allowance -- successfully saved.' : ''}
                                {newDAStatus === "Loading" && <PulseLoader color="#336573" size="9px" margin="4px" style={{marginTop:'2px'}} id="pulseLoader" />}
                            </div>
                        </Alert>
                    </ModalBody>
		          	<ModalFooter>
                      <Button color="primary" onClick={() => {this.props.dispatch(toggleDACreationStatus())}}>OK</Button>
		          	</ModalFooter>
		        </Modal>

                <Modal isOpen={showPrintReportsModal} toggle={() => {this.props.dispatch(togglePrintReportsModal(false))}} className={this.props.className}>
		        	<ModalHeader toggle={() => {this.props.dispatch(togglePrintReportsModal(false))}}>Debit Allowance Reports</ModalHeader>
		        	<ModalBody>
                        <CheckboxFilter
                            options={[]}
                            onChange={() => {}}
                            placeholder="" 
                            value={[]}
                            valueKey="value"
                            labelKey="value"
                            classname="DA-New-Input"
                            label="Select Report"
                            disabled={false}
                            hideOnSelect={true}
                            multi={false} />
                        <div className="DA-New-Brand-Input">
                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Debit Allowance</span>
                            <Input
                                style={{ height: '29px', borderRadius: '5px' }}
                                value={this.state.value}
                                onChange={(e) => {this.changeValue(e)}}
                                className="idHdr"
                                bsSize="lg"
                                name="da"
                                id="da"
                                disabled={true}
                                placeholder="" />
                        </div>
                        <div className="DA-New-Brand-Input">
                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Print Note &nbsp;&nbsp;</span>
                            <Input type="checkbox" name="check" id="exampleCheck"/>
                        </div>
                    </ModalBody>
		          	<ModalFooter>
                        <Print />
                        <Button color="secondary" onClick={() => {this.props.dispatch(togglePrintReportsModal(false))}}>Cancel</Button>
		          	</ModalFooter>
		        </Modal>
            </div>
        );
    }
}

New.propTypes = {}

const mapStateToProps = state => {
    return {
        dlaState: state.dlaState,
        newState: state.newState
    }
}

export default connect(mapStateToProps)(New);