import config from 'json-loader!./app.config.json';

function getEnvironmentVariables() { 
  var env = process.env.NODE_ENV;
  return config[env];
}

export function getAppServiceRouters() {
  return config["app_service_routers"];
}

export function getServiceRouter(routerKey) {
  var serviceRouters = getAppServiceRouters();
  return serviceRouters[routerKey];
}