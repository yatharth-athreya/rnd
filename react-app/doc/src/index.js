import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import './app/scss/oldBootstrap/portaltheme/css/bootstrap.min.css';
import './app/scss/oldBootstrap/portaltheme/css/bootstrap-theme.min.css';
import './app/scss/oldBootstrap/oldbundle.scss'
import 'font-awesome/css/font-awesome.min.css';

import App from './app/App';

class DADocument extends HTMLElement {
    constructor(props) {
        super(props);
    }

    connectedCallback() {
        try {
            if (this.errorMode) {
                throw new Error('Application failed at load');
            }
        } catch (e) {
            this.produceError(e);
            return;
        }

        console.log('da-document connected');
        this.render();
    }

    render() {
        const userDetails = this.getAttribute('userDetailsObj');
        render(<BrowserRouter><App userDetails={userDetails} title={this.title}/></BrowserRouter>, this);
    }

    disconnectedCallback() {
        console.log('da-document disconnected',);
    }
}

window.customElements.define('da-document-app', DADocument);