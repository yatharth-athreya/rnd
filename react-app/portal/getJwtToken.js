const jwt = require("jsonwebtoken");
const uuidv4 = require('uuid/v4');

function jwtEncryptor(jwtid, issuer, userId, issuedAt, fcRSAPrivateKey) {
    return new Promise((resolve, reject) => {
      let requestPayload = {
        jwtid: jwtid,
        issuer: issuer,
        userId: userId,
        issuedAt: issuedAt
      };
      let options = { algorithm: "RS512", noTimestamp: true };
  
      // Generate Digitally signed token with Rsa Private Key
      jwt.sign(requestPayload, fcRSAPrivateKey, options, (err, encryptedToken) => {
        if (err) {
          reject(err)
        } else {
          resolve('Bearer ' + encryptedToken)
        }
      })
    })
}

async function getToken(userTk, issuerName, fcRSAPrivateKey) {

    let issuedAt = Math.floor(new Date().getTime() / 1000)
    let issuer = issuerName
    let userId = userTk
    let jwtid = uuidv4()
  
    let encryptedToken = await jwtEncryptor(jwtid, issuer, userId, issuedAt, fcRSAPrivateKey)
  
    var headers = {
      "Authorization": encryptedToken,
      "Delivery-Date": issuedAt,
      "From": userId,
      "X-Correlation-ID": jwtid
    };
    return headers;
}

module.exports = getToken;