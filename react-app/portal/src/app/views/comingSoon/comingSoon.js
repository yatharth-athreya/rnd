import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './comingSoon.scss';

class ComingSoon extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                <img src="../../../../public/img/coming-soon.jpg" className="comingSoonImg" />
            </div>
        )
    }
}


export default ComingSoon