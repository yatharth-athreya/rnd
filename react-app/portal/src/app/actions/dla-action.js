import Notifications from 'react-notification-system-redux';
import moment from 'moment';
import axios from "axios";

let dlaResults = [
    {
        "id": "id",
        "application": "Application",
        "department": "Department",
        "effStartDate": "Effective Start Date",
        "effEndDate": "Effective End Date"                
    },
    {
        "id": "TKMA0GF",
        "application": "Allocation",
        "department": "10",
        "effStartDate": "10/15/2019",
        "effEndDate": "10/02/2025"                
    }
];

export const setAlerts = (uid, level) => {
    return dispatch => {
        dispatch(Notifications.hide(uid));
        level === 'info' ? dispatch(Notifications.show({uid, level: 'info', message: 'loading'})): 
        level === 'warning' ? dispatch(Notifications.warning({uid, message: 'No data found.', level, color: 'warning'})):
        level === 'error' ? dispatch(Notifications.error({uid, message: 'error', level, color: 'danger'})): '';
    }
}

export const setid = (value) => {
    return (dispatch, getState) => {
        dispatch({ type: "DLA_SET_ID", idValue: value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '').toUpperCase() });
    }
}

export const setApplication = (value) => {
    return (dispatch, getState) => {
        dispatch({ type: "DLA_SET_APPLICATION", applicationValue: value});
    }
}

export const setDepartment = (value) => {
    return (dispatch, getState) => {
        dispatch({ type: "DLA_SET_DEPARTMENT", departmentValue: value});
    }
}

export const getDepartmentByid = () => {
    return (dispatch, getState) => {
        const deptURL = `${fetchURL}` + '/api/getDepartment?app=FUSE_UI';
        dispatch(setAlerts(101, 'info'));
        axios.get(deptURL)
            .then( response => {
                if (response.status === 200){
                    dispatch({
                        type: 'DLA_SET_DEPARTMENT_LIST',
                        departmentList: response.data
                    })
                    dispatch(setAlerts(101, 'success'));
                } else {
                    dispatch(setAlerts(101, 'error'));
                }
            })
            .catch(() => {dispatch(setAlerts(101, 'error'))})
    }
}

export const clearDLA = () => {
    return (dispatch, getState) => {
        dispatch({ type: "DLA_CLEAR_RESULTS" });
    }
}

export const saveDLA = () => {
    return (dispatch, getState) => {
        let {dlaResults, idValue, applicationList, departmentList, effStartDate, effEndDate} = getState().dlaState;
        let newDLA = {
            "id": idValue,
            "application": applicationList.filter(e => e.isChecked).reduce((accumulator, currentValue, currentIndex, array) => accumulator + currentValue.value + (currentIndex === array.length - 1 ? "" : ", "), ''),
            "department": departmentList.filter(e => e.isChecked).reduce((accumulator, currentValue, currentIndex, array) => accumulator + currentValue.id + (currentIndex === array.length - 1 ? "" : ", "), ''),
            "effStartDate": moment(effStartDate).format("MM/DD/YYYY"),
            "effEndDate": moment(effEndDate).format("MM/DD/YYYY")                
        };
        dispatch({ type: "DLA_SAVE", newDLA });
    }
}

export const loadDLA = () => {
    return (dispatch, getState) => {
        dispatch({ type: "DLA_RESULTS", dlaResults });
    }
}

export const setEffDateRange = (startDate, endDate) => {
    return (dispatch, getState) => {
        dispatch({ type: "DLA_SET_EFF_DATE_RANGE", startDate, endDate });
    }
}