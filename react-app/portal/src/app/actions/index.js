import axios from "axios";

export const addUserDetails = (userDetails) => {
    return (dispatch, getState) => {
        dispatch({ type: "ADD_USER_DETAILS", userDetails });
    }
}
export function addPrefBgColor(color){
    return (dispatch, getState) => {
        dispatch({ type: "ADD_BG_COLOR", background_color: color });
    }
}
export function addPrefContrast(val){
    return (dispatch, getState) => {
        dispatch({ type: "ADD_CONTRAST", val });
    }
}
export function addPrefFsize(sizeVal){
    return (dispatch, getState) => {
        dispatch({ type: "ADD_FONT_SIZE", sizeVal });
    }
}

export function themeFinal(t) {
    return (dispatch, getState) => {
        dispatch({type:'NEW_THEME', t});
    }
}
export function addRowHeight(rowHeight) {
    return (dispatch, getState) => {
        dispatch({type:'ADD_ROW_HEIGHT', rowHeight});
    }
}
export const initializeHomePage = () => {
    return (dispatch, getState) => {
        let {userDetails} = getState().homePage.userData;
        let params = {
            "tk_id" : userDetails.userName,
            "limit": 100
        };
    }
}

export const toggleHistoryCards = (item) => {
    return (dispatch, getState) => {
    	let browsingHistoryCards = getState().homePage.userData.browsingHistoryCards;
		if (browsingHistoryCards.includes(item.page_name)) {
			browsingHistoryCards = browsingHistoryCards.filter(cardName => (cardName !== item.page_name));
		} else {
	    	browsingHistoryCards = browsingHistoryCards.concat(item.page_name);
	    }
        dispatch({ type: "SET_HISTORY_CARDS", browsingHistoryCards });
    }
}

export const addBrowsingHistory = (item) => {
    return (dispatch, getState) => {
        let {userData} = getState().homePage;
        let {browsingHistory, userDetails} = userData;
        browsingHistory = browsingHistory.filter(historyItem => (historyItem.page_name !== item.name));
        let params = {
            "tk_id" : userDetails.userName,
            "first_name": userDetails.firstName,
            "last_name": userDetails.lastName,
            "app_name" : item.app_name,
            "page_name" : item.name,
            "page_url" : item.url,
            "access_time" : item.access_time
        };
        browsingHistory = browsingHistory.concat(params);
        dispatch({ type: "ADD_BROWSING_HISTORY", browsingHistory });
    }
}