import React, { Component } from 'react';
import 'babel-polyfill';
import { Switch, Route, Redirect, withRouter} from 'react-router-dom';
import { Container } from 'reactstrap';
import Header from './components/Header/Header';
import Sidebar from './components/Sidebar/Sidebar';
import Breadcrumb from './components/Breadcrumb/Breadcrumb';
import Footer from './components/Footer/Footer';
import HomePage from './components/HomePage/HomePage';
import axios from 'axios';
import { BounceLoader } from 'halogenium';
import { connect } from 'react-redux';
import thunk from 'redux-thunk';
import IdleTimer from 'react-idle-timer';
import { addBrowsingHistory, addUserDetails } from "./actions";
import 'react-dates/lib/css/_datepicker.css';

import '../../scss/oldBootstrap/portaltheme/css/bootstrap.min.css';
import '../../scss/oldBootstrap/portaltheme/css/bootstrap-theme.min.css';
import '../../scss/oldBootstrap/oldbundle.scss'

import 'react-select/dist/react-select.css'
import 'react-virtualized/styles.css'
import 'react-virtualized-select/styles.css'

import { fuseAppUrl, opcAllocationUrl, opcAllocationAdminUrl, poAdminUrl, OptimizationPoUrl } from '../app/app.config';
import Home from './containers/Home/Home';
import DepartmentLevelAccess from './containers/Admin/DepartmentLevelAccess/DepartmentLevelAccess';
import DAAdmin from './containers/Admin/Admin';
import ARDocument from './containers/DA/Document/ARDocument';
import NewDocument from './containers/DA/Document/NewDocument';
import SearchDocument from './containers/DA/Document/SearchDocument';
import AcctPayable from './containers/DA/Approval/AcctPayable';
import Merchant from './containers/DA/Approval/Merchant';
import Page404 from './views/Pages/Page404/Page404';

import ComingSoon from './views/comingSoon/comingSoon';
import Dexie from 'dexie';

import { ThemeProvider } from 'theming';
import { handleCases } from '../themes/Themes';
import './../themes/Themes.scss';
import './../themes/BrandSearch.scss';

import reducers from './reducers/index';
import { createLogger } from 'redux-logger';
import { createStore, applyMiddleware, bindActionCreators } from 'redux';
import promise from 'redux-promise-middleware';
import { loadingBarMiddleware } from 'react-redux-loading-bar';

let middleware = [promise(), thunk, loadingBarMiddleware({
    promiseTypeSuffixes: ['START', 'SUCCESS', 'ERROR'],
})];

if (process.env.NODE_ENV !== 'production') {
    middleware.push(createLogger());
}

const createStoreWithMiddleware = applyMiddleware(...middleware)(createStore);
const store = createStoreWithMiddleware(reducers);
let modalBody = document.querySelector('body');
let theme;

class App extends Component {
  	constructor(props) {
    	super(props);
    	this.handleNavLinkClick = this.handleNavLinkClick.bind(this);
		this.getTheme = this.getTheme.bind(this);
		this.onActive = this._onActive.bind(this);
		this.onIdle = this._onIdle.bind(this);
		this._authUser = this._authUser.bind(this);
		this._addToken = this._addToken.bind(this);
		this.idleTimer = null;
    	this.state = {
			playing: true,
			userDetailsObj: null,
			appErrors: [],
			themes: '',
			timeout: 1 * 60 * 60 * 1000,
			remaining: null,
			isIdle: false,
			lastActive: null,
			elapsed: null
		};
	}

	componentWillMount() {
		this._addToken();
		axios.get('/udsvg')
		.then(res => {
			this.props.dispatch({ type: "ADD_USER_DETAILS", userDetails: res.data });
			this._authUser();
		});

		let appUrl = [];
		let ENV_VAR = [];
		axios.get('/epsvg')
		.then(res => {
			this.props.dispatch({ type: "ADD_EP", ep: res.data });
			ENV_VAR = res.data;

			if (true) {
				appUrl.push(`${ENV_VAR.DA_ADMIN_URL}`);
			}
			if (true) {
				appUrl.push(`${ENV_VAR.DA_APPROVAL_URL}`);
			}
			if (true) {
				appUrl.push(`${ENV_VAR.DA_DOCUMENT_URL}`);
			}
			
			for (let i = 0; i < appUrl.length; i++) {
				this.loadScssFile(`${appUrl[i]}/dist/index.styles.css`, "css");
			}
			for (let i = 0; i < appUrl.length; i++) {
				this.loadJsFile(`${appUrl[i]}/dist/index.bundle.js`, "js");
			}
		});
	}

	componentDidMount() {
		setTimeout(() => {this.setState({playing:false})}, 2000);		
	}
	
	componentWillReceiveProps(nextProps) {
		if((nextProps.location.pathname != this.props.location.pathname) && nextProps.location.pathname.indexOf("/imshome/po/") !== -1){
			this.props.dispatch({ type: "ADD_BG_COLOR", color: "light" });
		}
	}

	_authUser(){
		axios.get('/ausvg')
        .then( res => {
            axios.defaults.headers.common['Authorization'] = res.data['Authorization'] ? res.data['Authorization'] : '';
            axios.defaults.headers.common['Delivery-Date'] = res.data['Delivery-Date'] ? res.data['Delivery-Date'] : '';
            axios.defaults.headers.common['From'] = res.data['From'] ? res.data['From'] : '';
			axios.defaults.headers.common['X-Correlation-ID'] = res.data['X-Correlation-ID'] ? res.data['X-Correlation-ID'] : '';
			
			const fetchURL = this.props.ep.DAS_SERVICE_URL;
		
			this.props.dispatch({type: "DAS_SET_RACF_FS", status: "Loading"});
			axios.get(`${fetchURL}/v1/get-user-roles`)
			.then( res => {
				if (res.status === 200) {
					this.props.dispatch({type: "DAS_SET_RACF", data: res.data.result})
				} else {
					this.props.dispatch({type: "DAS_SET_RACF_FS", status: "Error"})
				}
			})
			.catch(() => {
				this.props.dispatch({type: "DAS_SET_RACF_FS", status: "Error"})
			})
        })
        .catch()
	}
	
	_addToken(){
		Dexie.exists('DasDataBase').then(function (exists) {
			Dexie.delete('DasDataBase');
			axios.get('/ausvg')
			.then( au => {
				var header = { appid:"DA_API",headers: au.data};
				var db = new Dexie("DasDataBase");
				db.version(1).stores({
					daui: `
					++id,
					appid,
					headers`,
				});
				db.daui.add(header).then();
			})
			.catch()
		});		
	}

	_onActive () {
		this.setState({ isIdle: false });
	}
	
	_onIdle () {
		const {ep} = this.props;
		window.location.href = ep.DAS_UI_ENTRYPOINT;
		this.setState({ isIdle: true });
	}
	  
	getTheme(preferences) {
		theme = handleCases(preferences);
		modalBody.getAttributeNode("class").value ='app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden '+theme;
		return theme;
	}  

  	loadScssFile(filename, filetype){
        var fileref = document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", filename)
        document.getElementsByTagName("head")[0].appendChild(fileref)
  	}

  	loadJsFile(filename, filetype){
    	let newStateErrorApps = this.state.appErrors;
        var fileref=document.createElement('script');
        fileref.setAttribute("type","text/javascript");
        fileref.setAttribute("async","");
        fileref.setAttribute("src", filename);
        fileref.onerror = function() {
          newStateErrorApps.push(this.src);
        }
        document.getElementsByTagName("body")[0].appendChild(fileref);
        this.setState({
          appErrors: newStateErrorApps
        })
  	}	

	handleNavLinkClick(item){
		this.props.dispatch(addBrowsingHistory(item));
	}

  	render() {
		const { userDetailsObj, appErrors } = this.state;
		const { preferences, userDetails } = this.props;
		let prefTheme = this.getTheme(preferences);		
		let theme = prefTheme && prefTheme.includes("gray") ? {background: '#2e2e2e', color: '#c8ced3', borderColor: '#6f7985'}: {color: 'black', background: 'white'};
		let rowHeightVal = preferences.rowHeight;
		let bodyPathName = this.props.location.pathname;
	
    	return (
			this.state.playing ? <BounceLoader color="rgb(32, 168, 216)" size="100px" style={{margin: '200px 50%'}} /> :
			
			<div className={`app ${(prefTheme)}`}>
				<ThemeProvider theme={theme}><Header userDetails={userDetailsObj} prefTheme={prefTheme} locationPath={this.props.location.pathname} /></ThemeProvider>
					<div className="app-body" style={{backgroundColor: (prefTheme.includes("gray") ? '#2e2e2e': '')}}>
						<ThemeProvider theme={theme}><Sidebar userDetails={userDetailsObj} handleNavLinkClick={this.handleNavLinkClick} {...this.props} /></ThemeProvider>
						<main className={`main ${(prefTheme)} rowHeight-${rowHeightVal}`} >
							<Breadcrumb />
							<Container fluid>
								<Switch>
									<Route exact path="/departmentlevelaccessadmin" name="Department Level Access" render={ props => <DAAdmin userDetails={userDetails}/>}/>
									<Route exact path="/assetrecoverydocument" name="Document - Asset Recovery" render={ props => <ARDocument userDetails={userDetails}/>}/>
									<Route exact path="/newdocument" name="Document - New" render={ props => <NewDocument userDetails={userDetails}/>}/>
									<Route exact path="/searchdocument" name="Document - Search" render={ props => <SearchDocument userDetails={userDetails}/>}/>
									<Route exact path="/acctpayableapproval" name="DAApproval - Acct Payable" render={ props => <AcctPayable userDetails={userDetails}/>}/>
									<Route exact path="/merchantapproval" name="DAApproval - Merchant" render={ props => <Merchant userDetails={userDetails}/>}/>						
									<Route path="/" name="Home" render={ props => <HomePage userDetails={userDetails}/>} />
									<Route path="/imshome/pagenotfound" component={Page404} />
                  					<Redirect to="/imshome/pagenotfound" />
								</Switch>
							</Container>
						</main>
					</div>
				<ThemeProvider theme={theme}><Footer /></ThemeProvider>
				<IdleTimer
					ref={ref => { this.idleTimer = ref }}
					onActive={this.onActive}
					onIdle={this.onIdle}
					timeout={this.state.timeout}
					startOnLoad />
			</div>
		);
    }
}

const mapStateToProps = state => {
	return {
    	defaultPreferences: state.homePage.userData.defaultPreferences,
    	preferences: state.homePage.userData.preferences,
		userDetails: state.homePage.userData.userDetails,
		ep: state.homePage.ep
  	};
};

export default withRouter(connect(mapStateToProps)(App));