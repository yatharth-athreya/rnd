const initialState = {
    selectedType: {"label": "Search By Buyer", "value": "Search By Buyer"},
    searchTypeOptions: [
        {
            "label": "Search By Buyer", 
            "value" : "Search By Buyer"
        },
        {
            "label": "Search By DMM", 
            "value" : "Search By DMM"
        },{
            "label": "Search By Debit Allowance", 
            "value" : "Search By Debit Allowance"
        },{
            "label": "Search By Debit Type", 
            "value" : "Search By Debit Type"
        },{
            "label": "Search By Department", 
            "value" : "Search By Department"
        },{
            "label": "Search By Price Change Doc", 
            "value" : "Search By Price Chagne Doc"
        },{
            "label": "Search By Status", 
            "value" : "Search By Status"
        }
    ]
}

export default (state= initialState, action) => {
    switch(action.type) {
        case "DASEARCH-SET-SEARCH-TYPE":
            return {
                ...state,
                selectedType: action.selectedType.selectedType
            }
        default:
            return state;
    }
}