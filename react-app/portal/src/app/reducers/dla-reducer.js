import moment from 'moment';

const initialState = {
    idValue: '',
    applicationList: [{"value": "Allocation"}, {"value": "Debit Allowance"}, {"value": "MR Eval"}, {"value": "Retail Order Creation"}].map(app => ({ ...app, isChecked: false })),
    departmentList: [],
    dlaResults : [],
    showDLAResults: true,
    effStartDate: null,
    effEndDate: null
}

export default (state = initialState, action) => {
    switch(action.type) {
        case "DLA_SET_ID":
            return {
                ...state,
                idValue: action.idValue
            }
        case "DLA_SET_APPLICATION":
            return {
                ...state,
                applicationList: state.applicationList.map(app => app.value === action.applicationValue.value ? action.applicationValue : app)
            }
        case "DLA_SET_DEPARTMENT":
            return {
                ...state,
                departmentList: state.departmentList.map(app => app.displayDesc === action.departmentValue.displayDesc ? action.departmentValue : app)
            }
        case "DLA_SET_DEPARTMENT_LIST":
            return {
                ...state,
                departmentList: action.departmentList.map(app => ({ ...app, isChecked: false }))
            }        
        case "DLA_SET_EFF_DATE_RANGE":
            return {
                ...state,
                effStartDate: action.startDate,
                effEndDate: action.endDate
            }
        case "DLA_SHOW_RESULTS":
            return {
                ...state,
                showDLAResults: true
            }
        case "DLA_CLEAR_RESULTS":
            return {
                ...state,
                idValue: '',
                applicationList: state.applicationList.map(app => ({ ...app, isChecked: false })),
                departmentList: state.departmentList.map(app => ({ ...app, isChecked: false })),
                effStartDate: null,
                effEndDate: null
            }
        case "DLA_SAVE":
            return {
                ...state,
                dlaResults: state.dlaResults.concat(action.newDLA)
            }
        case "DLA_RESULTS":
            return {
                ...state,
                dlaResults: action.dlaResults
            } 
        default:
            return state;
    }
}