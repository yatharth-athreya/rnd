import { combineReducers } from "redux";
import homePage from "./homePage-reducer";
import dlaState from "./dla-reducer";
import searchState from "./search-reducer";
import {reducer as notifications} from 'react-notification-system-redux';

export default combineReducers({
  dlaState,
  homePage,
  notifications,
  searchState
})
