import React, { Component } from 'react';
import { connect } from 'react-redux';
import CheckboxFilter from '../../../common/CheckBoxFilter/CheckBoxFilter';
import {Alert, Button, ButtonGroup, Input, Table, Badge, Card, CardBody, CardFooter, CardHeader, Col, Row, Collapse, Fade} from 'reactstrap';
import {setid, setApplication, getDepartmentByid, setDepartment, loadDLA, clearDLA, saveDLA, setEffDateRange} from '../../../../actions/dla-action';
import './DLA.scss';

import RGL, { WidthProvider } from "react-grid-layout";
import { PulseLoader } from 'halogenium';
const ReactGridLayout = WidthProvider(RGL);

import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import moment from 'moment';
import { DateRangePicker, isInclusivelyAfterDay } from 'react-dates';
import DLAGrid from './DLAGrid';

class DepartmentLevelAccess extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.toggleFade = this.toggleFade.bind(this);
        this.gridLayoutRef = React.createRef();
        this.state = {
            focusedInput: null,
            collapse: true,
            layout: [
                {"i": "DLA-AAA", "x": 0, "y": 0, "w": 12, "h": 2, "minW": 12, "minH": 2},
                {"i": "DLA-BBB", "x": 0, "y": 2, "w": 12, "h": 1, "minW": 12, "minH": 1}
            ]
        };
    }

    componentWillMount(){
        this.props.dispatch(loadDLA());
        this.props.dispatch(getDepartmentByid());
    }

    toggle() {
      this.setState({ collapse: !this.state.collapse });
    }
  
    toggleFade() {
      this.setState((prevState) => { return { fadeIn: !prevState }});
    }

    render() {
        const {idValue, applicationList, departmentList, dlaResults, showDLAResults, effStartDate, effEndDate} = this.props;
        return (
            <div className="DA-DLA">
                <ReactGridLayout ref={this.gridLayoutRef} className="layout" layout={this.state.layout} cols={12}>
                    <div key="DLA-AAA">
                        <div className="DA-DLA-child">
                            <div className="dla-child-header">New</div>
                            <hr />
                            <div>
                                <div className="DA-DLA-New" style={{display: 'flex', justifyContent: 'space-between'}}>
                                            <div className="DA-DLA-Filters">
                                                <span style={{ fontSize: '12px', fontWeight: 700 }}>id</span>
                                                <Input
                                                    style={{ height: '34px', borderRadius: '5px' }}
                                                    value={idValue}
                                                    onChange={(e) => {this.props.dispatch(setid(e.target.value))}}
                                                    className="idHdr"
                                                    bsSize="lg"
                                                    name="id"
                                                    id="id"
                                                    ref={this.id}
                                                    autoFocus={false}
                                                    disabled={false}
                                                    type="text"
                                                    maxLength={7}
                                                    placeholder="Enter id" />
                                            </div>
                                            <CheckboxFilter
                                                options={applicationList}
                                                onChange={(value) => {this.props.dispatch(setApplication(value))}}
                                                placeholder="Select Application" 
                                                value={applicationList.filter(e => e.isChecked)}
                                                valueKey="value"
                                                labelKey="value"
                                                classname="DA-DLA-Filters"
                                                label="Application"
                                                disabled={false}
                                                hideOnSelect={true}
                                                multi={false} />
                                            <CheckboxFilter
                                                options={departmentList}
                                                onChange={(value) => {this.props.dispatch(setDepartment(value))}}
                                                placeholder="Select Department" 
                                                value={departmentList.filter(e => e.isChecked)}
                                                valueKey="displayDesc"
                                                labelKey="displayDesc"
                                                classname="DA-DLA-Filters"
                                                label="Department"
                                                disabled={!idValue || idValue.length < 7}
                                                hideOnSelect={true}
                                                multi={false} />
                                            <div className={"DA-DLA-Filters"}>
                                                <span style={{ fontSize: '12px', fontWeight: 700 }} >{"Effective Start/End Date Range"}</span>
                                                <DateRangePicker
                                                    startDate={(effStartDate)} // momentPropTypes.momentObj or null,
                                                    startDateId="dla-effective-start-date" // PropTypes.string.isRequired,
                                                    endDate={effEndDate} // momentPropTypes.momentObj or null,
                                                    endDateId="dla-effective-end-date" // PropTypes.string.isRequired,
                                                    onDatesChange={({ startDate, endDate }) => {this.props.dispatch(setEffDateRange(startDate, endDate))}} // PropTypes.func.isRequired,
                                                    focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                                    onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                                                    disabled={false}
                                                    isOutsideRange={day => !isInclusivelyAfterDay(day, moment())}
                                                    readOnly
                                                />
                                            </div>
                                        </div>
                                        <div style={{display: 'flex', marginTop: '10px'}}>
                                            <ButtonGroup style={{marginLeft: 'auto'}}>
                                                <Button
                                                    style={{borderRadius:'4px', marginRight:'10px', backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                                    size="lg" 
                                                    color='secondary'
                                                    disabled={false}
                                                    onClick={() => {this.props.dispatch(clearDLA())}}>
                                                    <i className="fa fa-undo" aria-hidden="true"></i>{' Clear'}
                                                </Button>
                                                <Button
                                                    title = {false ? "Please select at least one product" : ""}
                                                    style={{borderRadius: '4px'}}
                                                    size="lg" 
                                                    color='primary'
                                                    disabled={false}
                                                    onClick={() => {this.props.dispatch(saveDLA())}}>
                                                    <i className="fa fa-save" aria-hidden="true"></i>{' Save'}
                                                </Button>
                                            </ButtonGroup>
                                        </div>
                            </div>
                        </div>
                    </div>
                    <div key="DLA-BBB">
                        <div className="DA-DLA-child">
                            <div className="dla-child-header">Results</div>
                            <hr />
                            <div className="DA-DLA-Results">
                                <DLAGrid />
                            </div>  
                        </div>
                    </div>
                </ReactGridLayout>
            </div>
        )
    }
}

DepartmentLevelAccess.propTypes = {}

const mapStateToProps = state => {
    return {
        idValue: state.dlaState.idValue,
        applicationList: state.dlaState.applicationList,
        departmentList: state.dlaState.departmentList,
        effStartDate: state.dlaState.effStartDate,
        effEndDate: state.dlaState.effEndDate,
        dlaResults: state.dlaState.dlaResults,
        showDLAResults: state.dlaState.showDLAResults,
    }
}

export default connect(mapStateToProps)(DepartmentLevelAccess);