import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Alert, Button, ButtonGroup, Input} from 'reactstrap';
import './New.scss';
import RGL, { WidthProvider } from "react-grid-layout";
import { PulseLoader } from 'halogenium';
const ReactGridLayout = WidthProvider(RGL);

class New extends Component {
    constructor(props) {
        super(props);
        this.gridLayoutRef = React.createRef();
        this.state = {
            layout: [
                {"i": "NewAAA", "x": 0, "y": 0, "w": 12, "h": 2, "minW": 12, "minH": 2},
                {"i": "NewBBB", "x": 0, "y": 2, "w": 12, "h": 1, "minW": 12, "minH": 1},
                {"i": "NewCCC", "x": 0, "y": 3, "w": 12, "h": 1, "minW": 12, "minH": 1},
                {"i": "NewDDD", "x": 0, "y": 4, "w": 12, "h": 0.5, "minW": 12, "minH": 0.5, "maxH": 0.5}
            ]
        };
    }
        
    render() {
        return (
            <div className="new-document">
                <ReactGridLayout ref={this.gridLayoutRef} className="layout" layout={this.state.layout} cols={12}>
                        <div key="NewAAA">
                            <div className="new-document-child">
                                <div id="new-document-header">Debit Allowance Entry/Maintenance</div>
                                <hr />
                                <div className="DA-New">
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Debit Allowance</span>
                                            <Input
                                                style={{ height: '34px', borderRadius: '5px' }}
                                                value={''}
                                                onChange={(e) => {}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="da"
                                                id="da"
                                                ref={this.id}
                                                
                                                disabled={false}
                                                type="text"
                                                maxLength={7}
                                                placeholder="Debit Allowance" />
                                        </div>
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Price Change Document</span>
                                            <Input
                                                style={{ height: '34px', borderRadius: '5px' }}
                                                value={''}
                                                onChange={(e) => {}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="pcd"
                                                id="pcd"
                                                ref={this.id}
                                                
                                                disabled={false}
                                                type="text"
                                                maxLength={7}
                                                placeholder="Price Change Document" />
                                        </div>
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Debit Type</span>
                                            <Input
                                                style={{ height: '34px', borderRadius: '5px' }}
                                                value={''}
                                                onChange={(e) => {}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="dt"
                                                id="dt"
                                                ref={this.id}
                                                
                                                disabled={false}
                                                type="text"
                                                maxLength={7}
                                                placeholder="Debit Type" />
                                        </div>
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Vendor</span>
                                            <Input
                                                style={{ height: '34px', borderRadius: '5px' }}
                                                value={''}
                                                onChange={(e) => {}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="da"
                                                id="da"
                                                ref={this.id}
                                                
                                                disabled={false}
                                                type="text"
                                                maxLength={7}
                                                placeholder="Vendor" />
                                        </div>
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Status</span>
                                            <Input
                                                style={{ height: '34px', borderRadius: '5px' }}
                                                value={''}
                                                onChange={(e) => {}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="pcd"
                                                id="pcd"
                                                ref={this.id}
                                                
                                                disabled={false}
                                                type="text"
                                                maxLength={7}
                                                placeholder="Status" />
                                        </div>
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Effective Date</span>
                                            <Input
                                                style={{ height: '34px', borderRadius: '5px' }}
                                                value={''}
                                                onChange={(e) => {}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="dt"
                                                id="dt"
                                                ref={this.id}
                                                
                                                disabled={false}
                                                type="text"
                                                maxLength={7}
                                                placeholder="Effective Date" />
                                        </div>
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Fiscal Month</span>
                                            <Input
                                                style={{ height: '34px', borderRadius: '5px' }}
                                                value={''}
                                                onChange={(e) => {}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="da"
                                                id="da"
                                                ref={this.id}
                                                
                                                disabled={false}
                                                type="text"
                                                maxLength={7}
                                                placeholder="Fiscal Month" />
                                        </div>
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Collection Method</span>
                                            <Input
                                                style={{ height: '34px', borderRadius: '5px' }}
                                                value={''}
                                                onChange={(e) => {}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="pcd"
                                                id="pcd"
                                                ref={this.id}
                                                
                                                disabled={false}
                                                type="text"
                                                maxLength={7}
                                                placeholder="Collection Method" />
                                        </div>
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Number of Days</span>
                                            <Input
                                                style={{ height: '34px', borderRadius: '5px' }}
                                                value={''}
                                                onChange={(e) => {}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="dt"
                                                id="dt"
                                                ref={this.id}
                                                
                                                disabled={false}
                                                type="text"
                                                maxLength={7}
                                                placeholder="Number of Days" />
                                        </div>
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Reason</span>
                                            <Input
                                                style={{ height: '34px', borderRadius: '5px' }}
                                                value={''}
                                                onChange={(e) => {}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="da"
                                                id="da"
                                                ref={this.id}
                                                
                                                disabled={false}
                                                type="text"
                                                maxLength={7}
                                                placeholder="Reason" />
                                        </div>
                                        <div className="DA-New-Input"></div>
                                        <div className="DA-New-Input"></div>
                                </div>
                            </div>
                        </div>
                        <div key="NewBBB">
                            <div className="new-document-child">
                                <div id="new-document-header">Proration Level</div>
                                <hr />
                                <div className="DA-New">
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Department</span>
                                            <Input
                                                style={{ height: '34px', borderRadius: '5px' }}
                                                value={''}
                                                onChange={(e) => {}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="da"
                                                id="da"
                                                ref={this.id}
                                                
                                                disabled={false}
                                                type="text"
                                                maxLength={7}
                                                placeholder="Department" />
                                        </div>
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Subclass</span>
                                            <Input
                                                style={{ height: '34px', borderRadius: '5px' }}
                                                value={''}
                                                onChange={(e) => {}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="pcd"
                                                id="pcd"
                                                ref={this.id}
                                                
                                                disabled={false}
                                                type="text"
                                                maxLength={7}
                                                placeholder="Subclass" />
                                        </div>
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Vendor E-mail</span>
                                            <Input
                                                style={{ height: '34px', borderRadius: '5px' }}
                                                value={''}
                                                onChange={(e) => {}}
                                                className="idHdr"
                                                bsSize="lg"
                                                name="dt"
                                                id="dt"
                                                ref={this.id}
                                                
                                                disabled={false}
                                                type="text"
                                                maxLength={7}
                                                placeholder="Vendor E-mail" />
                                        </div>
                                        <div className="DA-New-Input"></div>
                                    </div>
                            </div>
                        </div>
                        <div key="NewCCC">
                            <div className="new-document-child">
                                <div id="new-document-header">Debit Allowance Value</div>
                                <hr />
                                <div className="DA-New">
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Net Cost</span>
                                            <Input
                                                style={{ height: '34px', borderRadius: '5px' }}
                                                value={''}
                                                onChange={(e) => {}}
                                                className="netcost"
                                                bsSize="lg"
                                                name="netcost"
                                                id="netcost"
                                                ref={this.id}
                                                
                                                disabled={false}
                                                type="text"
                                                maxLength={7}
                                                placeholder="Net Cost" />
                                        </div>
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Markup %</span>
                                            <Input
                                                style={{ height: '34px', borderRadius: '5px' }}
                                                value={''}
                                                onChange={(e) => {}}
                                                className="markup"
                                                bsSize="lg"
                                                name="markup"
                                                id="markup"
                                                ref={this.id}
                                                
                                                disabled={false}
                                                type="text"
                                                maxLength={7}
                                                placeholder="Markup %" />
                                        </div>
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>Retail</span>
                                            <Input
                                                style={{ height: '34px', borderRadius: '5px' }}
                                                value={''}
                                                onChange={(e) => {}}
                                                className="retail"
                                                bsSize="lg"
                                                name="retail"
                                                id="retail"
                                                ref={this.id}
                                                
                                                disabled={false}
                                                type="text"
                                                maxLength={7}
                                                placeholder="Retail" />
                                        </div>
                                        <div className="DA-New-Input">
                                            <span style={{ fontSize: '12px', fontWeight: 700 }}>PO Number</span>
                                            <Input
                                                style={{ height: '34px', borderRadius: '5px' }}
                                                value={''}
                                                onChange={(e) => {}}
                                                className="ponumber"
                                                bsSize="lg"
                                                name="ponumber"
                                                id="ponumber"
                                                ref={this.id}
                                                
                                                disabled={false}
                                                type="text"
                                                maxLength={7}
                                                placeholder="PO Number" />
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div key="NewDDD">
                            <div className="new-document-child">
                                <div className="DA-New-Buttons">
                                <ButtonGroup>
                                    <Button
                                        title = {false ? "Please select at least one product" : ""}
                                        style={{}}
                                        size="lg" 
                                        color='primary'
                                        disabled={false}
                                        onClick={() => {}}>
                                        {'OK'}
                                    </Button>
                                    <Button
                                        style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                        size="lg" 
                                        color='secondary'
                                        disabled={false}
                                        onClick={() => {}}>
                                        {'OK + Repeat'}
                                    </Button>
                                    <Button
                                        style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                        size="lg" 
                                        color='secondary'
                                        disabled={false}
                                        onClick={() => {}}>
                                        {'Refresh'}
                                    </Button>
                                    <Button
                                        style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                        size="lg" 
                                        color='secondary'
                                        disabled={false}
                                        onClick={() => {}}>
                                        {'Notes'}
                                    </Button>
                                    <Button
                                        style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                        size="lg" 
                                        color='secondary'
                                        disabled={false}
                                        onClick={() => {}}>
                                        {'Brand'}
                                    </Button>
                                    <Button
                                        style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                        size="lg" 
                                        color='secondary'
                                        disabled={false}
                                        onClick={() => {}}>
                                        {'Delete'}
                                    </Button>
                                    <Button
                                        style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                        size="lg" 
                                        color='secondary'
                                        disabled={false}
                                        onClick={() => {}}>
                                        {'Cancel'}
                                    </Button>
                                </ButtonGroup>
                                </div>
                            </div>
                        </div>
                </ReactGridLayout>
            </div>
        );
    }
}


New.propTypes = {}

const mapStateToProps = state => {
    return {
        dlaState: state.dlaState
    }
}

export default connect(mapStateToProps)(New);