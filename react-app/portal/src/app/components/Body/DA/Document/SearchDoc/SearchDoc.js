import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Alert, Button, ButtonGroup, Input} from 'reactstrap';
import './SearchDoc.scss';
import RGL, { WidthProvider } from "react-grid-layout";
import { PulseLoader } from 'halogenium';
const ReactGridLayout = WidthProvider(RGL);
import "react-select/dist/react-select.css";
import "react-virtualized-select/styles.css";
import Select from "react-virtualized-select";
import {selectType} from "../../../../../actions/search-action";
import SearchGrid from "./SearchGrid";

class SearchDoc extends Component {
    constructor(props) {
        super(props);
        this.gridLayoutRef = React.createRef();
        this.state = {
            layout: [
                {"i": "search-doc-aaa", "x": 0, "y": 0, "w": 12, "h": 2, "minW": 12, "minH": 2},
                {"i": "search-doc-bbb", "x": 0, "y": 2, "w": 12, "h": 2, "minW": 12, "minH": 2},
                {"i": "search-doc-ccc", "x": 0, "y": 4, "w": 12, "h": 0.5, "minW": 12, "minH": 0.5, "maxH": 0.5}
            ]
        };
    }
        
    render() {
        const {searchTypeOptions, selectedType} = this.props;
        return (
            <div className="search-doc">
                <ReactGridLayout ref={this.gridLayoutRef} className="layout" layout={this.state.layout} cols={12}>
                        <div key="search-doc-aaa">
                            <div className="search-doc-child">
                                <div className="search-doc-child-header">Debit Allowance Search</div>
                                <div id="search-type-select"><Select labelKey='label' valueKey='value' options={searchTypeOptions} onChange={(selectedType) => this.props.selectType({ selectedType })} value={selectedType} clearable={false} /></div>
                                <hr />
                                <div className="search-doc-child-body"></div>
                            </div>
                        </div>
                        <div key="search-doc-bbb">
                            <div className="search-doc-child">
                                <div className="search-doc-child-header">Result</div>
                                <hr />
                                <div className="search-doc-child-body">
                                    <SearchGrid />
                                </div>
                            </div>
                        </div>
                        <div key="search-doc-ccc">
                            <div className="search-doc-child">
                                <div className="search-doc-buttons">
                                    <ButtonGroup>
                                        <Button
                                            style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                            size="lg" 
                                            color='secondary'
                                            disabled={false}
                                            onClick={() => {}}>
                                            {'Refresh'}
                                        </Button>
                                        <Button
                                            style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                            size="lg" 
                                            color='secondary'
                                            disabled={false}
                                            onClick={() => {}}>
                                            {'Maintenance'}
                                        </Button>
                                        <Button
                                            style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                            size="lg" 
                                            color='secondary'
                                            disabled={false}
                                            onClick={() => {}}>
                                            {'Details'}
                                        </Button>
                                        <Button
                                            style={{backgroundColor:'rgb(108, 117, 125)', borderColor:'rgb(108, 117, 125)', color:'white'}} 
                                            size="lg" 
                                            color='secondary'
                                            disabled={false}
                                            onClick={() => {}}>
                                            {'Cancel'}
                                        </Button>
                                    </ButtonGroup>
                                </div>
                            </div>
                        </div>
                    </ReactGridLayout>
            </div>
        );
    }
}

SearchDoc.propTypes = {}

const mapStateToProps = state => {
    return {
        dlaState: state.dlaState,
        selectedType: state.searchState.selectedType,
        searchTypeOptions: state.searchState.searchTypeOptions
    }
}

const mapDispatchToProps = dispatch => ({
    selectType: (v) => dispatch(selectType(v))
})

export default connect(mapStateToProps, mapDispatchToProps)(SearchDoc);