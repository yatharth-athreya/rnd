import React, { Component } from 'react';
import { AutoSizer, MultiGrid } from 'react-virtualized';
import './SearchGrid.scss';

export default class SearchGrid extends Component {
	constructor(props) {
    	super(props);
        this.state = {}
      
        this._gridHeight = this._gridHeight.bind(this);
        this._getGridData = this._getGridData.bind(this);
        this._cellRenderer = this._cellRenderer.bind(this);
    }
    
    _gridHeight (length) {
        const height = length * 30;
        const innerHeight = window.innerHeight - 300 <= 0 ? 300 : window.innerHeight - 300;
        return Math.min(height, innerHeight);
    }

    _getGridData(){
        const data = [
            {
                "rowIndex": 0, 
                "value":["Dept", "Dept Description", "Vendor Name", "Cost", "Retail", "Prc Chg", "DA Number", "Debit Type", "Reason", "Status", "Effective Date", "DMM Date", "A/P Date", "Completed Date"]
            },
            {
                "rowIndex": 1, 
                "value": Array(14).fill("")
            },
            {
                "rowIndex": 2, 
                "value": Array(14).fill("")
            },
            {
                "rowIndex": 3, 
                "value": Array(14).fill("")
            },
            {
                "rowIndex": 4, 
                "value": Array(14).fill("")
            }
        ];
        return data;
    }

	_cellRenderer({ columnIndex, key, rowIndex, style }) {
        const data = this._getGridData();
        const row = data.find(e => e.rowIndex === rowIndex);
        const rowClassName = rowIndex === 0 ? "cell-header" : "cell-body";
        return (
            <div key={key} style={style} className={`${rowClassName}`}>
                {row && row.value && row.value[columnIndex]}
            </div>
        );
    }
 
  	render() {        
        const data = this._getGridData();
        
        return (
            <div>
                {!!data.length ?
                    <div id="SearchGrid">
                        <AutoSizer disableHeight>
                            {({width}) => (
                                <MultiGrid
                                    ref={(ref) => this._grid = ref}
                                    cellRenderer={this._cellRenderer}
                                    columnWidth={150}
                                    columnCount={14}
                                    height={this._gridHeight(data.length) + 15}
                                    rowHeight={30}
                                    rowCount={data.length}
                                    width={width}
                                    fixedColumnCount={0}
                                    fixedRowCount={1}
                                    hideTopRightGridScrollbar
                                    hideBottomLeftGridScrollbar
                                /> 
                            )}
                        </AutoSizer>                        
                    </div>
                :null}
            </div>
		);
  	}
}