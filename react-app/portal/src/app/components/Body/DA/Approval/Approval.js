import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Nav, NavItem, NavLink } from 'reactstrap';
import './Approval.scss';

class Approval extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nav: "Acct Payable"
        };
        this.handleNav = this.handleNav.bind(this);
    }

    handleNav(nav){
        this.setState({nav});
    }
        
    render() {
        return (
            <div>
                <Nav>
                    <NavItem>
                        <NavLink href="#" onClick={() => {this.handleNav("Acct Payable")}}>Acct Payable</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="#" onClick={() => {this.handleNav("Merchant")}}>Merchant</NavLink>
                    </NavItem>
                </Nav>
                <hr />
                <div className="admin-document">
                    <div style={{display: 'flex'}}>
                        <span style={{fontSize: '18px', fontWeight: '800', flexGrow: '1'}}>{this.state.nav}</span>
                    </div>
                </div>
            </div>
        );
    }
}


Document.propTypes = {}

const mapStateToProps = state => {
    return {
        dlaState: state.dlaState
    }
}

export default connect(mapStateToProps)(Approval);