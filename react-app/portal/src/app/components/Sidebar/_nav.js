import { EdiHubUrlTransmission, EdiHubUrl } from '../../app.config';
export default {
  items: [
    {
      title: true,
      name: 'Debit Allowance Suite',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: 'dps-nav-title'             // optional class names space delimited list for title item ex: "text-center"
    },
    /*{
      name: 'ADMIN',
      icon: 'orange fas fa-brain',
      svg: "../../../../public/svg/admin.svg",
      children: [
        {
          name: 'Department Level Access',
          url: '/departmentlevelaccessadmin',
          icon: 'orange fas fa-signal',
          svg: "../../../../public/svg/remote-access.svg",
          icon: 'orange fas fa-braille',
          class: 'dps-nav-item',
          accessLevel: ['ADMIN','RW','RO'],
          parent: 'PSMP',
        }
      ]
    },*/
    {
      name: 'DEBIT ALLOWANCE',
      icon: 'orange fas fa-brain',
      svg: "../../../../public/svg/payment.svg",
      children: [
        {
          name: 'Debit Allowance Document',
          url: '/dashome/da/document',
          icon: 'orange fas fa-signal',
          svg: "../../../../public/svg/doc.svg",
          icon: 'orange fas fa-braille',
          class: 'dps-nav-item',
          accessLevel: ['ADMIN','RW','RO'],
          parent: 'PSMP',
          children: [
            {
              name: 'Asset Recovery',
              url: '/assetrecoverydocument',
              icon: 'orange fas fa-signal',
              svg: "../../../../public/svg/asset.svg",
              icon: 'orange fas fa-braille',
              class: 'da-nav-c-item',
              accessLevel: ['ADMIN','RW','RO'],
              parent: 'PSMP',
            },
            {
              name: 'New',
              url: '/newdocument',
              icon: 'orange fas fa-signal',
              svg: "../../../../public/svg/new.svg",
              icon: 'orange fas fa-braille',
              class: 'da-nav-c-item',
              accessLevel: ['ADMIN','RW','RO'],
              parent: 'PSMP',
            },
            {
              name: 'Search',
              url: '/searchdocument',
              icon: 'orange fas fa-signal',
              svg: "../../../../public/svg/search.svg",
              icon: 'orange fas fa-braille',
              class: 'da-nav-c-item',
              accessLevel: ['ADMIN','RW','RO'],
              parent: 'PSMP',
            }
          ]          
        },
        {
          name: 'Debit Allowance Approval',
          url: '/dashome/da/approval',
          icon: 'orange fas fa-signal',
          svg: "../../../../public/svg/approve.svg",
          icon: 'orange fas fa-braille',
          class: 'dps-nav-item',
          accessLevel: ['ADMIN','RW','RO'],
          parent: 'PSMP',
          children: [
            {
              name: 'Acct Payable',
              url: '/acctpayableapproval',
              icon: 'orange fas fa-signal',
              svg: "../../../../public/svg/accounting.svg",
              icon: 'orange fas fa-braille',
              class: 'da-nav-c-item',
              accessLevel: ['ADMIN','RW','RO'],
              parent: 'PSMP',
            },
            {
              name: 'Merchant',
              url: '/merchantapproval',
              icon: 'orange fas fa-signal',
              svg: "../../../../public/svg/merchant.svg",
              icon: 'orange fas fa-braille',
              class: 'da-nav-c-item',
              accessLevel: ['ADMIN','RW','RO'],
              parent: 'PSMP',
            }
          ]
        }
      ]
    }
  ]
};
