import React, {Component} from 'react';
import axios from 'axios';
import { Badge, DropdownItem, DropdownMenu,Tooltip, DropdownToggle, Nav, NavbarBrand, NavbarToggler, Dropdown, NavItem, NavLink } from 'reactstrap';
import './Header.scss';
import BadgeIcon from '../BadgeIcon'

import socketIOClient from "socket.io-client";
import { userInfo } from 'os';

import Preferences from '../preferences/Preferences';
import { withTheme } from 'theming';
import { handleCases } from '../../../themes/Themes';
import { connect } from 'react-redux';
import { themeFinal } from "../../actions";

class Header extends Component {
	constructor(props) {
		super(props);
		this.toggle = this.toggle.bind(this);
		this.state = {
			dropdownOpen: false,
			tooltipOpen: false,
			onlineUsers: []
		};
	}

	toggle() {
		this.setState({
			dropdownOpen: !this.state.dropdownOpen
		});
	}

	toggleToolTip = () => {
		this.setState({
			tooltipOpen: !this.state.tooltipOpen
		});
	}

	sidebarToggle(e) {
		e.preventDefault();
		document.body.classList.toggle('sidebar-hidden');
	}

	sidebarMinimize(e) {
		e.preventDefault();
		document.body.classList.toggle('sidebar-minimized');
	}

	mobileSidebarToggle(e) {
		e.preventDefault();
		document.body.classList.toggle('sidebar-mobile-show');
	}

	asideToggle(e) {
		e.preventDefault();
		document.body.classList.toggle('aside-menu-hidden');
	}

	render() {
		let { preferences, defaultPreferences, prefTheme } = this.props;
		return (
			<header className={`app-header navbar ${prefTheme}`}>
				<NavbarToggler className="d-lg-none" style={{fontSize: '24px'}} onClick={this.mobileSidebarToggle}>&#9776;</NavbarToggler>
				<NavbarToggler className="d-md-down-none" style={{fontSize: '24px'}} onClick={this.sidebarToggle}>&#9776;</NavbarToggler>
				<NavbarBrand style={{borderBottom: "none"}} href="#"></NavbarBrand>
				<Nav className="ml-auto" navbar>
					{/*<Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
						<DropdownToggle className="displayOnlineUsers"> 
							<BadgeIcon content={!!this.state.onlineUsers && this.state.onlineUsers.length > 0 ? this.state.onlineUsers.length : 1} />
						</DropdownToggle>
						<DropdownMenu className="user_online_dropdownmenu" >
							<DropdownItem header tag="div" className="text-center"><strong>Online Users</strong></DropdownItem>
							<div className="online_user_outer">
								{
									this.state.onlineUsers.length > 0 && this.state.onlineUsers.map(ac => {
									return(<DropdownItem key={ac.userId}>
										<i className="fa fa-user fa_user_size">
										<div className="user_count">{ac.count}</div>
										</i> 
										{`${ac.firstName} ${ac.lastName}` }
										<span className="users_dot_circle">
										</span>
									</DropdownItem>)
									})
								}
							</div>
						</DropdownMenu>
					</Dropdown>*/}
					<span className="d-md-down-none">
						<span className="userNameContainer">
							<i id="TooltipExample" className="fa fa-user-circle"></i>
							Hello {this.props.userDetails != null ? this.props.userDetails.firstName + " - " + this.props.userDetails.userName + " (" + ((this.props.userDetails.userGroup.includes("DA-Admin-Preview") || (this.props.userDetails.userGroup.includes("DA-Admin"))) ? "Admin" : "User") + ")" : ''}
						</span>
					</span>
					<Preferences userDetails={this.props.userDetails} locationPath={this.props.locationPath} defaultPreferences={defaultPreferences} preferences={preferences}></Preferences>    
				</Nav>
			</header>
		);
	}
}

const mapStateToProps = state => {
  	return {
		defaultPreferences: state.homePage.userData.defaultPreferences,
		preferences: state.homePage.userData.preferences,
		userDetails: state.homePage.userData.userDetails
	};
};
export default withTheme(connect(mapStateToProps)(Header));