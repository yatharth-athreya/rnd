import React, { Component } from 'react'
import IdleTimer from 'react-idle-timer'
import moment from 'moment';
 
export default class CheckIdleTime extends Component {
  constructor(props) {
    super(props)
    this.idleTimer = null
    this.onAction = this._onAction.bind(this)
    this.onActive = this._onActive.bind(this)
    this.onIdle = this._onIdle.bind(this)
  }
 
  render() {
    return (
      <div>
        <IdleTimer
          ref={ref => { this.idleTimer = ref }}
          element={document}
          onActive={this.onActive}
          onIdle={this.onIdle}
          onAction={this.onAction}
          debounce={250}
          timeout={1000 * 60 * 1} />
        {/* your app here */}
      </div>
    )
  }
 
  _onAction(e) {
    console.log('user did something', e, 'this.idleTimer.getRemainingTime()', this.idleTimer.getRemainingTime(), 'this.idleTimer.getLastActiveTime()', this.idleTimer.getLastActiveTime())
    let gh = moment(new Date(this.idleTimer.getLastActiveTime())).format('LLLL'); 
    // this.props.userLogOut(gh,"active")
    this.idleTimer.reset();
  }
 
  _onActive(e) {
    // console.log('user is active', e,'time remaining', this.idleTimer.getRemainingTime()
  }
 
  _onIdle(e) {
    let gh = moment(new Date(this.idleTimer.getLastActiveTime())).format('LLLL'); 
    // this.props.userLogOut(gh, "notActive")
    console.log('user is idle', e,'last active', this.idleTimer.getLastActiveTime(), 'nnnnnnn ', gh)
  }
}