import React from 'react';
import PropTypes from 'prop-types';
import { Row } from 'reactstrap';

const UnAuthorizedAccess = ({errorMsg}) => (
    <Row className=''>                                                                            
        <p className="error-msg">          
             {errorMsg}                
        </p>
   </Row>
);

UnAuthorizedAccess.propTypes = { errorMsg: PropTypes.string };

export default UnAuthorizedAccess;