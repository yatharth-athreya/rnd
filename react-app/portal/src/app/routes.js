const routes = {
  '/': 'Home',
  '/departmentlevelaccessadmin': 'Department Level Access',
  '/assetrecoverydocument': 'Debit Allowance NEW Asset Recovery',
  '/newdocument': 'Debit Allowance NEW Entry',
  '/searchdocument': 'Debit Allowance Search',
  '/acctpayableapproval': 'Debit Allowance AP Approval',
  '/merchantapproval': 'Debit Allowance Merchant Approval',
};
export default routes;
