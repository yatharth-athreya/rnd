import React, { Component } from 'react';
import UnAuthorizedAccess from '../../../components/UnAuthorizedAccess/UnAuthorizedAccess';

class ARDocument extends Component {
    render() {
        const { userDetails } = this.props;
        const env = process.env.NODE_ENV;
        let allowAccess = false;
        if (env === 'production') {
            allowAccess = ["DA-Admin", "DA-ARU"].includes(userDetails.userGroup);
        } else {
            allowAccess = ["DA-Admin-Preview", "DA-ARU-Preview"].includes(userDetails.userGroup);
        }
        return(
            allowAccess ?
                <da-document-app></da-document-app>
            : 
                <UnAuthorizedAccess errorMsg= "id is not assigned to the user group. Please contact CPS leadership team to add your TK to the group." />
        )
    }
}

export default ARDocument;