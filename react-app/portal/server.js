require('dotenv').config({path: process.env.ENV_VAR_PATH});

const swStats = require('swagger-stats');
const http = require('http');
const express = require('express');
const app = express();
const prometheus = require("prometheus-wrapper");

//session mechanism start
var session = require('cookie-session'),
    auth = require('./auth');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compression = require('compression');
var morgan = require('morgan');
var xFrameOptions = require('x-frame-options');

const enableAuthentication = !process.env.DISABLE_AUTHENTICATION;

app.use(morgan('combined'));
app.use(compression());
app.use(cookieParser());
app.use(xFrameOptions());
app.use(bodyParser.urlencoded({
  	extended: true
}));
app.use(bodyParser.json());
app.use(function (req, res, next) {
	console.log('%s %s %s', req.method, req.url, req.path);  
	res.header('Cache-Control', ' no-cache, no-store, must-revalidate, pre-check=0, post-check=0, max-age=0, and s-maxage=0');
	res.header('Expires', '0');
	res.header('Pragma', 'no-cache');
	next()
});

const jwt = require("jsonwebtoken");
const RSAPrivateKey = `${process.env.DAS_UI_RSA_PRIVATE_KEY}`;
const uuidv4 = require('uuid/v4');

async function getTokenHeader(userId, userGroup) {
	let issuedAt = Math.floor(new Date().getTime() / 1000);
	let jwtid = uuidv4();
	let requestPayload = {
		jti: jwtid,
		issuer: 'DA_UI',
		userId: userId,
		userRole: userGroup
	};
  	let encryptedToken = await jwtEncryptor(requestPayload);
	var headers = {
		"Authorization": encryptedToken,
		"Delivery-Date": issuedAt,
		"From": userId,
		"X-Correlation-ID": jwtid
	};
  	return headers;
}

var helmet = require('helmet')
app.use(helmet())

app.use(session({
	name: 'session-das',
	secret: "process.env.COOKIE_KEY",
	maxAge: 1800000,
}));

app.get('/health', function (req, res) {
  	res.json({ status: "UP" });
});

if (!!enableAuthentication) {
  	app.use(auth.initialize());
  	app.use(auth.session());
  	app.post('/login/callback', auth.authenticate('saml', { failureRedirect: '/', failureFlash: true }), function (req, res) {
		var d = new Date();
		var d_time = d.getTime();
		var url_to_redirect = '/';
		if (!!req.session && req.session.passport && req.session.passport.user && Object.keys(req.session.passport.user).length) {
			console.log('login-callback user: - ', req.session.passport.user);
			req.session.passport.user.time_sys = d_time;
			url_to_redirect = req.session.url_to_redirect || '/';
			req.session.url_to_redirect = '';
		}
		res.redirect(url_to_redirect);
    });
  	app.use(swStats.getMiddleware({}));
  	app.get('/login', function(req, res) {
		var url_to_redirect = req.query.url_to_redirect;
		if (url_to_redirect.startsWith('/')) {
			if (!!req.session) {
				console.log('login user: - ', req.session);
				req.session.url_to_redirect = url_to_redirect;
			}
			auth.authenticate('saml',
			{
				additionalParams: {'RelayState': url_to_redirect},
				failureRedirect: '/',
				failureFlash: true
			})(req, res);
		} else {
			res.redirect('/');
		}
	});
  	app.use(auth.protected);
}

app.get('/logout', async (req, res) => {
	await req.logout();
	// req.user = null;
	// req.session = null;
	// req.sessionOptions.maxAge = 0;
  	return res.redirect('/');
});

app.use(express.static('./'));
app.use(express.static('dist'));

app.get('/metrics', function(req, res) {
  	res.end(prometheus.getMetrics());
});

app.get('/epsvg', function (req, res) {
	res.json({ 
		keyVal: process.env.COOKIE_KEY,
		DA_ADMIN_URL: process.env.DA_ADMIN_URL,
		DA_APPROVAL_URL: process.env.DA_APPROVAL_URL,
		DA_DOCUMENT_URL: process.env.DA_DOCUMENT_URL,
		DAS_SERVICE_URL: process.env.DAS_SERVICE_URL,
		DAS_UI_ENTRYPOINT: process.env.DAS_UI_ENTRYPOINT
	});
});

if (!!enableAuthentication) {
	app.get('/udsvg', function (req, res) {
    	res.json({ 
			userName: req.session.passport.user.id, 
			userGroup: req.session.passport.user.pqdb_grp,
			firstName: req.session.passport.user.firstName, 
			lastName: req.session.passport.user.lastName
    	});
  	});
	app.get('/ausvg', async function (req, res) {
		var tokenHeader = await getTokenHeader(req.session.passport.user.id, req.session.passport.user.pqdb_grp);
		res.json(tokenHeader)
	});
	app.get('/*', auth.protected, function(req, res) {
		res.sendFile(`${__dirname}/dist/index.html`);
	});
} else {
	app.get('/udsvg', function (req, res) {
		res.json({ 
			userName: process.env.USER_NAME,
			userGroup: process.env.USER_GROUP,
			firstName: process.env.FIRST_NAME, 
			lastName: process.env.LAST_NAME
		})
	});
	app.get('/ausvg', async function (req, res) {
		var tokenHeader = await getTokenHeader(process.env.USER_NAME, process.env.USER_GROUP);
		res.json(tokenHeader)
	});
	app.get('/*', function(req, res) {
		res.sendFile(`${__dirname}/dist/index.html`);
	});
}

function jwtEncryptor(requestPayload) {
	return new Promise((resolve, reject) => {
		let options = { expiresIn: '24h', algorithm: "RS512" };

		// Generate Digitally signed token with Rsa Private Key
		jwt.sign(requestPayload, RSAPrivateKey, options, (err, encryptedToken) => {
			if (err) {
				reject(err)
			} else {
				resolve('Bearer ' + encryptedToken)
			}
		})
	})
}

app.use(function(req, res, next) {
	if (req.originalUrl && req.originalUrl.split("/").pop() === 'favicon.ico') {
    	return res.sendStatus(204);
  	}
  	return next();
});

const hostPort = process.env.PORT || 17022;
process.title = process.argv[2];

var server = http.createServer(app);
server.listen(hostPort, () => {
	console.log("env", process.env);
});